var readline = require('readline');
var config = require('./package.json');

const input = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const VERSION = config['version'];

console.log('You are about to deploy to PRODUCTION environment.');
input.question('Please enter current ' + config['name'] + ' version: ', function(answer) {
  input.close();
  if (answer === VERSION) {
    console.log('\033[32mVersion is valid, proceeding to deploy...\033[0m\n');
    process.exit(0);
  } else {
    console.log('\033[31mVersion is NOT valid!\033[0m');
  }
  process.exit(1);
});
