
interface DOMHelpers<Datum> {
  parents(before: d3.Selection<any>): d3.Selection<any>[];

  parent(): d3.Selection<Datum>;
  parent(className: string): d3.Selection<Datum>;

  add(tag: string, className: string): d3.Selection<Datum>;
  div(className: string): d3.Selection<Datum>;
  span(className: string): d3.Selection<Datum>;
  input(className: string): d3.Selection<Datum>;
  tooltip(className: string): d3.Selection<Datum>;
}

declare module d3 {
  import Anchor = brightar.ui.Anchor;
  import Point = brightar.ui.Point;

  interface Selection<Datum> extends DOMHelpers<Datum> {
    highlighted(value: boolean): d3.Selection<Datum>;
    pin(at: Anchor): d3.Selection<Datum>;

    once(event: string, listener: (data: Datum) => void): d3.Selection<Datum>

    modify(className: string): d3.Selection<Datum>;
    modify(className: string, enabled: boolean): d3.Selection<Datum>;
    modify(className: string, enabled: (datum: Datum, index: number) => boolean): d3.Selection<Datum>;

    element(className: string): d3.Selection<Datum>;

    selectElement(elementName: string): d3.Selection<Datum>;
    selectElements(elementName: string): d3.Selection<Datum>;
  }

  module selection {
    module enter {
      export var prototype: Enter<any>;
    }

    interface Enter<Datum> extends DOMHelpers<Datum> {}
  }

  interface Transition<Datum> {
    styleTween(property: string, tween: () => (t: number) => string): Transition<Datum>;
    transition(): Transition<Datum>;
  }
}
