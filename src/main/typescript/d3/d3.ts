
type D3Type = d3.Selection<any>;

const _baseClass = function(node: HTMLElement) {
  return node.className && node.className.split(' ')[0] || '';
};

function _parents(before: d3.Selection<any>): d3.Selection<any>[] {
  if (!this.empty()) {
    const node: HTMLElement = this.node();
    const parent: d3.Selection<any> = d3.select(node && node.parentNode);

    if (!parent.empty() && node !== before.node()) {
      return [d3.select(node)].concat(parent.parents(before))
    }

    return [d3.select(node)];
  }

  return [];
}

const _parent = function(className: string = ''): D3Type {
  const node = this.node();
  if (node === null) {
    return this;
  } else {
    const parent = d3.select(node.parentNode);
    if (className === '') {
      return parent;
    } else {
      if (checkClass(node, className)) {
        return this;
      } else {
        return _parent.call(parent, className);
      }
    }
  }

  function checkClass(node: HTMLElement, className: string): boolean {
    const base = _baseClass(node);
    return base.indexOf(className) === base.length - className.length;
  }
};

const _add = function(type: string, className: string ): D3Type {
  return this.append(type).classed(className, true);
};

const _addTag = function(tag: string): (type: string) => D3Type {
  return function(className: string): D3Type {
    return _add.call(this, tag, className);
  }
};

const _div = _addTag('div');
const _span = _addTag('span');
const _input = _addTag('input');

const _tooltip = function(tooltip: string): D3Type {
  return this.attr('title', tooltip);
};

const _modify = function(modifier: string, enable: any = true): D3Type {
  const node = this.node();
  if (node === null) {
    return this;
  } else {
    const base = _baseClass(node);
    if (typeof enable === 'boolean') {
      return this.classed(base + '--' + modifier, enable);
    } else {
      return this.classed(base + '--' + modifier, (d: any, i: number) => enable(d, i));
    }
  }
};

const _element = function(element: string): D3Type {
  const node = this.node();
  if (node === null) {
    return this;
  } else {
    const base = _baseClass(node);
    return this.div(base + '__' + element);
  }
};

const _selectd3 = function(funcName: string, elementName: string): D3Type {
  const node = this.node();
  if (node === null) {
    return this;
  } else {
    const base = _baseClass(node);
    return this[funcName]('*[class^="' + base + '__"][class*="__' + elementName + '"]');
  }
};

const _selectElement = function(elementName: string): D3Type {
  return _selectd3.call(this, 'select', elementName);
};

const _selectElements = function(elementName: string): D3Type {
  return _selectd3.call(this, 'selectAll', elementName);
};


d3.selection.prototype.parents = _parents;
d3.selection.prototype.parent = _parent;
d3.selection.enter.prototype.parent = _parent;

d3.selection.prototype.add = _add;
d3.selection.enter.prototype.add = _add;

d3.selection.prototype.div = _div;
d3.selection.enter.prototype.div = _div;

d3.selection.prototype.span = _span;
d3.selection.enter.prototype.span = _span;

d3.selection.prototype.input = _input;
d3.selection.enter.prototype.input = _input;

d3.selection.prototype.tooltip = _tooltip;
d3.selection.enter.prototype.tooltip = _tooltip;

d3.selection.prototype.modify = _modify;

d3.selection.prototype.element = _element;

d3.selection.prototype.selectElement = _selectElement;

d3.selection.prototype.selectElements = _selectElements;
