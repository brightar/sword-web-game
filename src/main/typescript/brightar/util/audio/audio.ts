namespace brightar.util.audio {

  const _registry: { [file: string] : HTMLAudioElement } = {};


  function _audio(file: string): HTMLAudioElement {
    if (_registry[file] === undefined) {
      _registry[file] = new Audio(file);
    }

    return <HTMLAudioElement> _registry[file];
  }

  export function play(name: string): void {
    try {
      _audio('sound/' + name + '.mp3').play()
    } catch (e) {}
  }
}
