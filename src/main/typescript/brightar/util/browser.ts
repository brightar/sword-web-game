namespace brightar.util {
  export function browser(): {browser: string, version: number} {
    return _browser(navigator.userAgent);
  }

  function _browser(userAgent: string): {browser: string, version: number} {
    const regexps: {[key: string]: RegExp}[] = [
      {'Opera': /OPR\/(\d+)\./},
      {'IE': /Edge\/(\d+)\./},
      {'Chrome': /Chrome\/(\d+)\./},
      {'Safari': /Version\/(\d+)\..*Safari\/\d+\./},
      {'Firefox': /Firefox\/(\d+)\./},
      {'IE': /MSIE (\d+)/},
      {'IE': /Trident.*rv:(\d+)/}
    ];

    var browsers = regexps.map((test) => {
      const name = Object.keys(test)[0];
      const regex = test[name];
      const tested = userAgent.match(regex);
      return tested === null ? undefined : {
        browser: name,
        version: Number(tested[1])
      }
    }).filter((el) => el !== undefined);

    browsers.push({
      browser: 'Unknown',
      version: -1
    });

    return browsers[0];
  }

  export function testBrowser(): boolean {
    const opera   = _browser("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.73 Safari/537.36 OPR/34.0.2036.25");
    const safari  = _browser("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/601.4.4 (KHTML, like Gecko) Version/9.0.3 Safari/601.4.4");
    const chrome  = _browser("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36");
    const firefox = _browser("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:43.0) Gecko/20100101 Firefox/43.0");
    const ie9     = _browser("Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C)");
    const ie10    = _browser("Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C)");
    const ie11    = _browser("Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; rv:11.0) like Gecko");
    const ie12    = _browser("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240");
    const unknown = _browser("Unknown stuff");

    return opera.browser === 'Opera' && opera.version === 34 &&
           safari.browser === 'Safari' && safari.version === 9 &&
           chrome.browser === 'Chrome' && chrome.version === 48 &&
           firefox.browser === 'Firefox' && firefox.version === 43 &&
           ie9.browser === 'IE' && ie9.version === 9 &&
           ie10.browser === 'IE' && ie10.version === 10 &&
           ie11.browser === 'IE' && ie11.version === 11 &&
           ie12.browser === 'IE' && ie12.version === 12 &&
           unknown.browser === 'Unknown' && unknown.version === -1;
  }
}
