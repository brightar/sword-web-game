namespace brightar.util.store {

  export function saveFlag(key: string, value: boolean): void {
    if (value) localStorage.setItem(key, '');
    else localStorage.removeItem(key);
  }

  export function loadFlag(key: string): boolean {
    return localStorage.getItem(key) !== null;
  }

  export function load(key: string, orElse: string = ''): string {
    const data: string = localStorage.getItem(key);

    if (data === null) {
      if (orElse !== '') {
        localStorage.setItem(key, orElse)
      }

      return orElse;
    }

    return data;
  }
}
