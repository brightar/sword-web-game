namespace brightar.util {

  export type Hash = { [key: string]: any };

  export function uid(): string {
    return Math.random().toString(36).substr(2);
  }

  export function range(start: number, count: number): number[] {
    return Array.apply(0, Array(count)).map(
        (_: any, index: number) => index + start);
  }

  export function clone(obj: Hash, copy: Hash = {}): Hash {
    for (var attr in obj) {
      copy[attr] = obj[attr];
    }

    return copy;
  }

  export function copy(from: Hash, to: Hash): Hash {
    return clone(from, to);
  }

  export function decodeUrlParams(): Hash {
    var result: Hash = {};
    if (location.href.split('?').length === 2) {
      const pairs = location.href.split('?')[1].split('&');
      pairs.forEach((pair) => {
        const [key, value] = pair.split('=');
        result[key] = decodeURIComponent(value);
      });
    }

    return result;
  }

  export function arrRemove<T>(array: T[], element: T): void {
    for (var i = array.length - 1; i >= 0; i -= 1) {
      if (array[i] === element) {
        array.splice(i, 1);
      }
    }
  }
}
