namespace brightar.service {

  export interface IState<Data> {
    read(): Data[]
    item(index: number): Data
    touch(index: number): void
    update(callback: Callback): void
  }
}