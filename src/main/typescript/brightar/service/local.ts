namespace brightar.service {

  export class Local<Data> implements IState<Data> {

    private _callbacks: Callback[] = [];
    private _data: Data[] = [];

    constructor(input: Data | Data[]) {
      this._data = [].concat(input).filter((i) => i !== null);
    }

    item(index: number): Data {
      return this._data[index];
    }

    read(): Data[] {
      return this._data;
    }

    touch(index: number): void {
      const callbacks: Callback[] = this._callbacks;
      this._callbacks = [];

      callbacks.forEach((callback) => callback())
    }

    update(callback: Callback): void {
      this._callbacks.push(callback);
    }
  }
}
