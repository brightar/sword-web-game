namespace brightar.service {

  export interface Processor<Data> {
    (item: Data, update: Callback): void
  }

  export function process<Data>(state: IState<Data>,
                                processor: Processor<Data>): void {

    function update(index: number): Callback {
      return () => state.touch(index)
    }

    const data: Data[] = state.read();

    for (var i: number = 0; i < data.length; i += 1) {
      processor(data[i], update(i))
    }

    state.update(() => process(state, processor));
  }
}
