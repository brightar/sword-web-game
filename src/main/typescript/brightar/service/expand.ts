namespace brightar.service {

  export class Expand<I, O> implements IState<O> {

    private _index: { [index: number]: number } = {};

    constructor(private _base: IState<I>,
                private _binder: Map<I, O | O[]>) {}

    item(index: number): O {
      return this._state[index];
    }

    read(): O[] {
      return this._state;
    }

    touch(index: number): void {
      this._base.touch(this._index[index]);
    }

    update(callback: Callback): void {
      this._base.update(callback);
    }

    private get _state(): O[] {
      this._index = {};

      const result: O[] = [];
      const input: I[] = this._base.read();

      for (var i: number = 0; i < input.length; i += 1) {
        const output: O[] = []
            .concat(this._binder(input[i]))
            .filter((i) => i !== null);

        for (var o: number = 0; o < output.length; o += 1) {
          this._index[result.length] = i;
          result.push(output[o])
        }
      }

      return result;
    }
  }
}
