namespace brightar.ui {

  export interface IWidget<State> {
    getClass(): string[]

    attach(node: d3.Selection<State>): void
    redraw(state: State, node: d3.Selection<State>, touch: Callback): void
    detach(node: d3.Selection<State>): void
  }
}