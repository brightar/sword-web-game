namespace brightar.ui {

  export interface Point {
    x: number,
    y: number
  }
}