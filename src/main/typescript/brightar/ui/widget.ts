namespace brightar.ui {

  export class Widget<State> implements IWidget<State> {

    constructor(private _class: string[],
                private _events: string[] = []) {}

    getClass(): string[] {
      return this._class;
    }

    attach(node: d3.Selection<State>): void {
      this._init(node);

      for (var event of this._events) {
        this._listen(event, node);
      }
    }

    redraw(state: State, node: d3.Selection<State>, touch: Callback): void {
      this._update(state, node);
      this._idle(state, touch);
    }

    detach(node: d3.Selection<State>): void {
      for (var event of this._events) {
        node.on(event, null);
      }

      this._destroy(node.remove());
    }

    protected _init(node: d3.Selection<State>): void {}
    protected _idle(state: State, touch: Callback): void {}

    protected _event(event: string,
                     state: State,
                     target: d3.Selection<any>): boolean {

      if (event === 'click') return this._click(state, target);
      if (event === 'mousedown') return this._mousedown(state, target);
      if (event === 'keydown') return this._keydown(state, target);
      if (event === 'keypress') return this._keypress(state, target);

      return true;
    }

    protected _update(state: State, node: d3.Selection<State>): void {}
    protected _destroy(node: d3.Selection<State>): void {}

    private _listen(event: string,
                    node: d3.Selection<State>): void {

      node.on(event, (state) => {
        const parents: d3.Selection<any>[] =
            d3.select(d3.event.target).parents(node);

        for (var target of parents) {
          if (this._event(event, state, target)) break;
        }
      })
    }

    protected _click(state: State, target: d3.Selection<any>): boolean {
      return true;
    }

    protected _keypress(state: State, target: d3.Selection<any>): boolean {
      return true;
    }

    protected _keydown(state: State, target: d3.Selection<any>): boolean {
      return true;
    }

    protected _mousedown(state: State, target: d3.Selection<any>): boolean {
      return true;
    }
  }
}