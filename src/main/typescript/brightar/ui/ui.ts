namespace brightar.ui {

  import IState = brightar.service.IState;

  export const root: d3.Selection<any> = d3.select(document.body);

  export function app<Data>(core: IWidget<Data>, state: IState<Data>): void {
    const classname: string = _classname(core.getClass());
    const data: Data = state.item(0);
    const node: d3.Selection<Data> = root.datum(data);

    if (!node.classed(classname)) {
      core.attach(node.attr('class', _classname(core.getClass())));
    }

    core.redraw(data, node, () => state.touch(0));

    state.update(() => app(core, state));
  }

  export function widget<Data>(root: d3.Selection<any>,
                               core: IWidget<Data>,
                               state: IState<Data>): void {

    const classname: string = _classname(core.getClass());
    const selector: string = _selector(core.getClass());

    function update(data: Data, index: number): void {
      core.redraw(data, d3.select(this), () => state.touch(index));
    }

    function destroy(): void {
      core.detach(d3.select(this));
    }

    function init(): void {
      core.attach(d3.select(this).attr('class', classname));
    }

    const node: d3.selection.Update<Data> =
        root.selectAll(selector).data(state.read());

    node.enter().append('div').each(init);
    node.exit().each(destroy);
    node.each(update);

    state.update(() => widget(root, core, state));
  }

  const _highlighted: Element[] = [];

  function _selector(classes: string[]): string {
    return '.' + classes.join('.');
  }

  function _classname(classes: string[]): string {
    return classes.join(' ');
  }

  function _resetOverlay(): void {
    d3.select(document.body)
      .classed('overlay', _highlighted.length > 0);
  }

  function _processHighlight(nodes: Element[], value: boolean): void {
    for (var i: number = nodes.length - 1; i >= 0; i -= 1) {
      if (nodes[i] !== null) {
        const node: Element = nodes[i];
        const currentIndex: number = _highlighted.indexOf(node);
        const style: CSSStyleDeclaration = getComputedStyle(node);
        const mounted: boolean =
            style.position === 'absolute' ||
            style.position === 'fixed' ||
            style.position === 'relative';

        if (value && currentIndex == -1) {
          d3.select(node).style(mounted ? {
            'z-index': 1001
          } : {
            'z-index': 1001,
            'position': 'relative'
          }).classed('highlighted', value);

          _highlighted.push(node);
        }

        if (!value && currentIndex != -1) {
          d3.select(node).style({
            'z-index': null
          }).classed('highlighted', value);

          _highlighted.splice(currentIndex, 1);
        }
      }
    }
  }

  function _highlight<T>(value: boolean): d3.Selection<T> {
    _processHighlight(Array.prototype.concat.apply([], this), value);
    _resetOverlay();

    return this
  }

  function _rect(target: d3.Selection<any>): ClientRect {
    return Array.prototype.concat.apply([], target)
        .map((node: Element) => node.getBoundingClientRect())
        .reduce((a: ClientRect, b: ClientRect) => _rectSum(a, b), _zeroRect())
  }

  function _rectSum(a: ClientRect, b: ClientRect): ClientRect {
    const bottom: number = Math.max(a.bottom, b.bottom);
    const right: number = Math.max(a.right, b.right);
    const left: number = Math.min(a.left, b.left);
    const top: number = Math.min(a.top, b.top);

    return {
      height: bottom - top,
      width: right - left,

      top: top,
      bottom: bottom,

      left: left,
      right: right
    }
  }

  function _anchor(target: d3.Selection<any>, anchor: Anchor): Point {
    const rect: ClientRect = _rect(target);
    const hh: number = rect.height / 2;
    const hw: number = rect.width / 2;

    switch (anchor) {
      case Anchor.TOP_LEFT:  return { x: rect.left,      y: rect.top };
      case Anchor.TOP:       return { x: rect.left + hh, y: rect.top };
      case Anchor.TOP_RIGHT: return { x: rect.right,     y: rect.top };

      case Anchor.LEFT:   return { x: rect.left,      y: rect.top + hw };
      case Anchor.CENTER: return { x: rect.left + hh, y: rect.top + hw };
      case Anchor.RIGHT:  return { x: rect.right,     y: rect.top + hw };

      case Anchor.BOTTOM_LEFT:  return { x: rect.left,      y: rect.bottom };
      case Anchor.BOTTOM:       return { x: rect.left + hh, y: rect.bottom };
      case Anchor.BOTTOM_RIGHT: return { x: rect.right,     y: rect.bottom };
    }

    return _zeroPoint();
  }

  function _pin<T>(at: Anchor): d3.Selection<T> {
    const origin: Point = _anchor(this, at);

    return d3.select(document.body).append('div').datum(this.datum()).style({
      'position': 'absolute',
      'top': origin.y + 'px',
      'left': origin.x + 'px'
    });
  }

  function _zeroRect(): ClientRect {
    return {
      height: 0,
      width: 0,

      top: Infinity,
      bottom: 0,

      left: Infinity,
      right: 0
    }
  }

  function _zeroPoint(): Point {
    return { x: 0, y: 0 }
  }

  function _once<T>(event: string, listener: (d: T) => void): d3.Selection<T> {
    return this.on(event, (data: T) => {
      this.on(event, null);

      listener(data);
    });
  }

  d3.selection.prototype.pin = _pin;
  d3.selection.prototype.once = _once;
  d3.selection.prototype.highlighted = _highlight;
}
