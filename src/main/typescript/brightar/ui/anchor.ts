namespace brightar.ui {
  export enum Anchor {
    TOP_LEFT,
    TOP,
    TOP_RIGHT,

    LEFT,
    CENTER,
    RIGHT,

    BOTTOM_LEFT,
    BOTTOM,
    BOTTOM_RIGHT
  }
}