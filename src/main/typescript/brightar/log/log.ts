namespace brightar.log {

  import util = brightar.util;

  var _context: Hash = {};

  /**
   * uid, browser type, etc
   */
  export function context(data: Hash = {}) {
    _context = data;
  }

  export function event(event: string, data: Hash = {}): void {
    var data: Hash = util.copy(data, util.clone(_context));
    data['date'] = new Date().getTime();

    _nginx(event, JSON.stringify(data));
  }

  function _nginx(name: string, data: string = ""): void {
    d3.select('body')
      .append('img')
      .attr('src', '/log/' + name + '?' + data)
      .on('load', function() { d3.select(this).remove().on('load', null) });
  }
}
