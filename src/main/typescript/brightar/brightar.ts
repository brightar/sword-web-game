namespace brightar {

  export interface Callback {
    (...any: any[]): void
  }

  export interface Map<I, O> {
    (result: I): O
  }

  export function nop(): void {

  }
}