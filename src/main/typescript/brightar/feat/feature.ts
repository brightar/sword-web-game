namespace brightar.feat {

  export class Feature implements IFeature {

    constructor(private _sources: Feature[] = []) {}

    private _event: Callback = () => {};

    idle(event: Callback): void {
      this._event = event;

      this._sources.forEach((feature) => feature.idle(event))
    }

    protected _touch(...any: any[]): void {
      this._event();
    }
  }
}