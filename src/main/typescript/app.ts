import uid = brightar.util.uid;
import load = brightar.util.store.load;
import Local = brightar.service.Local;
import Hash = brightar.util.Hash;

import IPlatform = sword.api.platform.IPlatform;
import Root = sword.view.Root;

import User = sword.model.User;
import Account = sword.feat.Account;
import Gift = sword.feat.Gift;
import Player = sword.feat.role.Player;
import Explorer = sword.feat.role.Explorer;
import Shop = sword.feat.view.Shop;
import Navigation = sword.feat.view.Navigation;
import Keyboard = sword.feat.view.Keyboard;
import Invite = sword.feat.Invite;
import Platform = sword.feat.Platform;
import AutoUpdate = sword.feat.AutoUpdate;

function init(platform: string, options: Hash = {}): void {
  sword.api.platform.init(platform, options, (id, data) => {
    const browser = brightar.util.browser();
    brightar.log.context({
      'platform': platform,
      'browser': load('browser', uid()),
      'page': uid(),
      'user': id
    });

    brightar.log.event(sword.log.APP_OPEN, brightar.util.copy({
      'browser': browser.browser, 'browserVersion': browser.version,
      'browserFull': navigator.userAgent
    }, data));

    window.onerror = (msg, url, line, col, error) => {
      const browser = brightar.util.browser();
      return brightar.log.event(sword.log.ERROR, {
        'msg': msg, 'url': url,
        'line': line, 'col': col, 'error': error,
        'browser': browser.browser, 'version': browser.version
      });
    };

    const explorer: Explorer = new Explorer(id);
    const keyboard: Keyboard = new Keyboard();
    const account: Account = new Account(id);
    const navigation: Navigation = new Navigation(explorer);
    const shop: Shop = new Shop(account, sword.api.platform.bundles(), explorer);
    const gift: Gift = new Gift(id, account);
    const player: Player = new Player(id, account, navigation, keyboard, explorer);
    const invite: Invite = new Invite(id, account, new Platform());
    const autoUpdate = new AutoUpdate();

    const user: User = new User(id,
        account, gift, player, shop, navigation, keyboard, explorer, invite);

    const userService: Local<User> = new Local(user);

    brightar.ui.app(new Root(userService), userService);

    user.init();
    autoUpdate.init();
  });
}
