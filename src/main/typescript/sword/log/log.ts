namespace sword.log {

  export const ACCOUNT_BALANCE_CHANGE: string = 'account/balance-change';
  export const ACCOUNT_ORDER_START: string = 'account/order-start';
  export const ACCOUNT_ORDER_INVOICE: string = 'account/order-invoice';
  export const ACCOUNT_ORDER_FAIL: string = 'account/order-fail';
  export const ACCOUNT_ORDER_COMPLETE: string = 'account/order-complete';

  export const SOCIAL_PAYMENT_START: string = 'social/payment-start';
  export const SOCIAL_PAYMENT_COMPLETE: string = 'social/payment-complete';
  export const SOCIAL_PAYMENT_FAIL: string = 'social/payment-fail';

  export const PLAYER_GAME_START: string = 'player/game-start';
  export const PLAYER_GAME_CONTINUE: string = 'player/game-continue';
  export const PLAYER_WORD_OPEN: string = 'player/word-open';
  export const PLAYER_GIFT: string = 'player/gift';

  export const QUEST_HIDE: string = 'quest/hide';
  export const QUEST_SHOW: string = 'quest/show';
  export const QUEST_STEP: string = 'quest/step';
  export const QUEST_COMPLETE: string = 'quest/step';

  export const APP_OPEN: string = 'app/open';
  export const APP_SHOP_OPEN: string = 'app/shop-open';
  export const APP_SHOP_CLOSE: string = 'app/shop-close';

  export const INVITE_FRIEND: string = 'invite/friend';
  export const INVITE_ACCEPT: string = 'invite/accept';

  export const ERROR: string = 'error';
}
