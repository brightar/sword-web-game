namespace sword.feat {

  import Feature = brightar.feat.Feature;

  /**
   * Возможности социальной сети.
   *
   * - Получение данных профиля пользователя.
   */
  export class Platform extends Feature {

    private _cache: { [user: string]: api.platform.Profile } = {};
    private _buffer: api.platform.User[] = null;

    constructor() {
      super();
    }

    profile(user: api.platform.User): api.platform.Profile {
      if (this._cache[user] === undefined) {

        if (this._buffer === null) {
          this._buffer = [user];

          setTimeout(() => this._sync(), 0)
        } else {
          this._buffer.push(user);
        }

        return sword.api.platform.PROFILE_DUMMY;
      }

      return this._cache[user];
    }

    private _sync(): void {
      if (this._buffer !== null) {
        api.platform.populate(this._buffer, (profiles) => {
          for (var profile of profiles) {
            this._cache[profile.user] = profile;
          }
          this._touch();
        })
      }
    }
  }
}
