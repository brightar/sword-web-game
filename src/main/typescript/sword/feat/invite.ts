namespace sword.feat {

  import Feature = brightar.feat.Feature;
  import Player = sword.feat.role.Player;

  /**
   * Возможность приглашения в игру.
   *
   * - Отсылка приглашений друзьям.
   * - Выдача подарков пригласившим.
   * - Получения списка откликов.
   * - Получение подарков за отклик.
   */
  export class Invite extends Feature {
    private _accepted: api.invite.User[] = [];
    private static _TIMER: number =
        Math.floor((1 + 4 * (Math.random())) * 60 * 1000);

    constructor(private _id: api.invite.User,
                private _account: Account,
                private _platform: Platform) {

      super([_platform]);
    }

    init(): void {
      this.check();
      setTimeout(() => this.init(), Invite._TIMER);
    }

    get accepted(): api.platform.Profile[] {
      return this._accepted.map((user) => this._platform.profile(user));
    }

    accept(): void {
      const invitedBy = api.platform.invitedBy();
      if (invitedBy) {
        api.invite.invitedBy(invitedBy, this._id);
      }
      api.invite.succeed(this._id);
    }

    check(): void {
      api.invite.check(this._id, (accepted) =>
          this._touch(this._accepted = accepted));
    }

    send(): void {
      api.platform.invite("Интересные сканворды в Игре Слов!",
          (friends) => {
            api.invite.invite(this._id, friends);
            brightar.log.event(sword.log.INVITE_FRIEND, {
              count: friends.length
            });
          });
    }

    reward(friend: api.invite.User): void {
      api.invite.reward(this._id, friend, (balance) => {
        this._touch(brightar.util.arrRemove(this._accepted, friend));
        this._account.balance = balance;
        brightar.log.event(sword.log.INVITE_ACCEPT, {
          amount: this.amount()
        });
      });
    }

    amount(): number {
      return api.platform.starsForFriend();
    }
  }
}
