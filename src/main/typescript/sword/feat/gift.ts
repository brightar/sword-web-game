namespace sword.feat {

  import Feature = brightar.feat.Feature;

  /**
   * Возможность использования подарка.
   *
   * - Отображение текущего размера и время до увеличения
   * - Получение подарка
   */
  export class Gift extends Feature {
    private _updateTime: api.gift.Time = 0;
    private _timeRemain: api.gift.Time = 0;
    private _size: api.gift.Size = 0;

    constructor(private _user: api.gift.User,
                private _account: Account) {
      super();
    }

    get timeRemain(): api.gift.Time {
      return this._updateTime + this._timeRemain - Date.now();
    }

    get size(): api.gift.Size {
      return this._size;
    }

    set gift(gift: api.gift.Gift) {
      this._touch([this._size, this._timeRemain] = gift,
          this._updateTime = Date.now());
    }

    take(): void {
      if (this._size > 0) {
        brightar.log.event(sword.log.PLAYER_GIFT, { 'size': this._size });

        api.gift.take(this._user, (balance, gift) => {
          if (gift[0] !== -1) {
            this._account.balance = balance;
            this.gift = gift;
          }
        });
      }
    }

    check(): void {
      api.gift.check(this._user, (gift) => {
        this.gift = gift;
        this._touch();
        setTimeout(() => this.check(), Math.max(this.timeRemain + 1000, 0));
      });
    }
  }
}
