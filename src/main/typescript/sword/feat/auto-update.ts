namespace sword.feat {

  import Feature = brightar.feat.Feature;

  /**
   * Автообновление клиента.
   */
  export class AutoUpdate extends Feature {
    private static _MAX: number = Math.floor(180 * 60 * 1000 +
        Math.random() * 60 * 60 * 1000);
    private static _TIMER: number = 10 * 60 * 1000;

    private _time: number = 0;
    private _start: number = 0;
    private _timer: number = 0;

    constructor() {
      super([]);
    }

    init(): void {
      window.onblur = () => {
        if (this._start === 0) {
          this._start = new Date().getTime();
        }

        this.check();
      };

      window.onfocus = () => {
        this._time = 0;
        this._start = 0;
        clearTimeout(this._timer);
      };
    }

    check(): void {
      if (new Date().getTime() - this._start > AutoUpdate._MAX) {
        location.reload();
      }

      this._timer = setTimeout(() => this.check(), AutoUpdate._TIMER);
    }
  }
}
