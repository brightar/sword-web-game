namespace sword.feat {

  import Feature = brightar.feat.Feature;
  import Callback = brightar.Callback;

  /**
   * Возможность использования звездочек.
   *
   * - Отображение текущего количества звездочек и искорок (баланса).
   * - Проверка текущего баланса.
   * - Использование одной зведочки.
   * - Обмен новой игры на звездочки.
   */
  export class Account extends Feature {

    private _letters: number = 0;
    private _actions: number = 0;
    private static ERROR_INVOICE: string = 'error-invoice';

    constructor(private _user: api.account.User) {
      super();
    }

    set balance(balance: api.account.Balance) {
      if (this._letters !== balance[0]) {
        brightar.log.event(sword.log.ACCOUNT_BALANCE_CHANGE, {
          'balance': balance
        });
      }

      this._touch([this._letters, this._actions] = balance);
    }

    get letters(): number {
      return this._letters;
    }

    get actions(): number {
      return this._actions;
    }

    buyPuzzle(player: role.Player, complete: Callback,
              cancel: Callback = brightar.nop): void {
      api.puzzle.makeOrder(this._user, (invoice) => {
        if (invoice !== Account.ERROR_INVOICE) {
          api.account.puzzlePayment(this._user, invoice, (balance) => {
            if (balance[1] !== -1) {
              api.puzzle.commitOrder(this._user, invoice, (game) => {
                if (game !== null) {
                  complete(this.balance = balance, player.startGame(game));
                }
              });
            }
          });
        } else {
          cancel();
        }
      });
    }

    useLetter(): void {
      this._touch(this._letters -= 1);

      api.account.letterPayment(this._user,
          (balance) => this.balance = balance);
    }

    orderBundle(bundle: model.view.ShopBundle, complete: Callback) {
      brightar.log.event(sword.log.SOCIAL_PAYMENT_START, {
        'bundle': bundle
      });

      api.platform.buyLetters(bundle, () => {
        brightar.log.event(sword.log.SOCIAL_PAYMENT_COMPLETE);
        this.check(complete);
      }, () => {
        brightar.log.event(sword.log.SOCIAL_PAYMENT_FAIL);
        this.check(complete);
      })
    }

    check(complete: Callback = brightar.nop): void {
      api.account.check(this._user,
          (balance) => {
            this.balance = balance;
            complete();
          });
    }
  }
}
