namespace sword.feat.view {

  import platform = sword.api.platform;
  import Feature = brightar.feat.Feature;
  import ShopBundle = sword.model.view.ShopBundle;
  import Event = sword.model.quest.quests.Event;

  export type ShopCallback = (bought: boolean) => void

  export class Shop extends Feature {
    private static _DEFAULT_REASON: string = 'Купить звездочки';

    private _isOpened: boolean = false;
    private _minPrice: number = 0;
    private _reason: string = Shop._DEFAULT_REASON;

    private _shopCallback: ShopCallback = (b) => {};

    constructor(private _account: Account, private _bundles: ShopBundle[],
                private _explorer: Explorer) {
      super();
      this._bundles = _bundles.sort((a, b) => a.price - b.price);
    }

    get account(): Account {
      return this._account;
    }

    get bundles(): ShopBundle[] {
      return this._bundles;
    }

    get isOpened(): boolean {
      return this._isOpened;
    }

    get minStars(): number {
      return this._minPrice;
    }

    get reason(): string {
      return this._reason;
    }

    order(bundle: ShopBundle): void {
      const complete = () => {
        this.leave(true);
        this._explorer.explore(Event.PURCHASE);
      };

      if (bundle.discount !== 100) {
        this._account.orderBundle(bundle, complete);
      } else {
        complete();
      }
    }

    enter(reason: string = Shop._DEFAULT_REASON, minPrice: number = 0,
          callback: ShopCallback = () => {}): void {
      brightar.log.event(sword.log.APP_SHOP_OPEN);
      this._explorer.explore(Event.SHOP_OPEN);

      this._reason = reason;
      this._minPrice = minPrice;
      this._touch(this._isOpened = true);
      this._shopCallback = callback;
    }

    leave(bought: boolean): void {
      brightar.log.event(sword.log.APP_SHOP_CLOSE);

      this._touch(this._isOpened = false);
      this._shopCallback(bought);
      this._shopCallback = () => {};
      this._minPrice = 0;
    }

    addFake(): void {
      this._bundles.unshift(new ShopBundle(0, 10, 0, 100, 'shop/bundle0.svg'));
    }

    hasFake(): boolean {
      return this._bundles.some((bundle) => bundle.discount === 100);
    }

    removeFake(): void {
      this._bundles = this._bundles.filter((bundle) => bundle.discount !== 100);
    }
  }
}
