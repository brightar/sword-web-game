namespace sword.feat.view {

  import platform = sword.api.platform;
  import Feature = brightar.feat.Feature;
  import Page = sword.model.Page;
  import Event = sword.model.quest.quests.Event;

  export class Navigation extends Feature {
    private _page: string = sword.model.Page.PRELOADER;

    constructor(private _explorer: Explorer) {
      super()
    }

    set page(name: string) {
      this._touch(this._page = name);
      if (name === Page.HISTORY) {
        this._explorer.explore(Event.HISTORY_OPEN);
      }
    }

    get page(): string {
      return this._page;
    }
  }
}
