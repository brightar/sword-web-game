namespace sword.feat.view {

  import Feature = brightar.feat.Feature;

  export class Keyboard extends Feature {
    private _focusTarget: sword.model.IFocusTarget = null;

    constructor() {
      super()
    }

    blur(): void {
      this._focusTarget && this._focusTarget.blur();
      this._touch(this._focusTarget = null);
    }

    focus(target: sword.model.IFocusTarget): void {
      if (this._focusTarget !== target) {
        this._focusTarget && this._focusTarget.blur();
        this._touch(this._focusTarget = target);
      }
    }

    applyKey(code: string): void {
      this._focusTarget && this._focusTarget.insertChar(code);
    }
  }
}
