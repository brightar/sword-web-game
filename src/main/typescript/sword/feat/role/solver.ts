namespace sword.feat.role {

  import Feature = brightar.feat.Feature;
  import Callback = brightar.Callback;
  import Position = sword.model.game.Position;
  import Event = sword.model.quest.quests.Event;

  /**
   * Возможность разгадывать головоломку.
   *
   * - Использование подсказки.
   * - Ввод символов, фокусировка.
   * - Переключение выделения.
   */
  export class Solver extends Feature implements sword.model.IFocusTarget {

    constructor(private _user: api.game.User,
                private _game: model.game.Game,
                private _player: Player,
                private _account: Account,
                private _keyboard: Keyboard,
                private _explorer: Explorer) {
      super()
    }

    get game(): model.game.Game {
      return this._game;
    }

    get player(): Player {
      return this._player;
    }

    useHint(position: model.game.Position): void {
      if (this._account.letters > 0) {
        this._game.selectLetter(position, false);

        if (this._game.isCurrentLetter(position)) {
          this._explorer.explore(Event.HINT_USED);
          this._account.useLetter();
          this._game.openLetter(position);
          this._save();
          this._touch();
        }
      }
    }

    selectWord(clue: Position): void {
      this._game.selectWord(clue, true);
      this._explorer.explore(Event.CLUE_SELECTED);
      this._touch();
    }

    selectLetter(letter: Position): void {
      this._game.selectLetter(letter, true);
      this._touch();
    }

    focus(): void {
      this._keyboard.focus(this);
    }

    blur(): void {
      this._game.clearSelection();
      this._touch();
    }

    insertChar(code: string): void {
      if (code === 'up') {
        this._game.selectPrev(false);
      } else if (code === 'down') {
        this._game.selectNext(false)
      } else if (code === 'left') {
        this._game.selectPrev(true);
      } else if (code === 'right') {
        this._game.selectNext(true);
      } else if (/[А-Яа-я ]/.test(code)) {
        const words: string[] = this._game.insertChar(code);

        if (words.length > 0) {
          this._explorer.explore(Event.CLUE_SOLVED);
          this._save(words);
        }
      }

      this._touch();
    }

    private _save(words: string[] = []): void {
      brightar.log.event(sword.log.PLAYER_WORD_OPEN, {
        'game': this._game.id,
        'words': words,
        'is_won': this._game.isWon
      });

      api.game.save(this._user, this._game.id, this._game.puzzle, words.length,
          this._game.isWon, (balance) => this._account.balance = balance);
    }
  }
}
