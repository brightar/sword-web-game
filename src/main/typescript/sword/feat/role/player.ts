namespace sword.feat.role {

  import Feature = brightar.feat.Feature;
  import Callback = brightar.Callback;
  import Event = sword.model.quest.quests.Event;

  /**
   * Возможность перелючаться между играми.
   *
   * - Загрузка имеющихся игр.
   * - Начало новой игры.
   * - Продолжение игры.
   */
  export class Player extends Feature {

    private _hintReady: boolean = false;
    private _games: model.game.Game[] = [];
    private _solver: Solver = null;

    constructor(private _user: api.game.User,
                private _account: Account,
                private _navigation: Navigation,
                private _keyboard: Keyboard,
                private _explorer: Explorer) {
      super();
    }

    get solver(): Solver {
      return this._solver;
    }

    get isHintReady(): boolean {
      return this._hintReady;
    }

    set isHintReady(value: boolean) {
      this._touch(this._hintReady = value);
      if (value) {
        this._explorer.explore(Event.STAR_PRESSED);
      }
    }

    get allGames(): model.game.Game[] {
      return this._games;
    }

    continueGame(game: model.game.Game, byUser: boolean = false): void {
      if (byUser) {
        brightar.log.event(sword.log.PLAYER_GAME_CONTINUE, { 'game': game.id });
        this._explorer.explore(Event.GAME_CONTINUE);
      }
      api.game.touch(game.id);

      this._open(game);
      this._touch();
    }

    startGame(game: api.game.Game): void {
      brightar.log.event(sword.log.PLAYER_GAME_START, {
        game: game.id,
        price: api.platform.puzzlePrice()
      });

      this._explorer.explore(Event.GAME_ORDER);
      api.game.touch(game.id);

      this._open(this._game(game, this._games.length));
      this._games.push(this._solver.game);

      this._touch();
    }

    loadGames(complete: Callback): void {
      api.game.load(this._user, (games) =>
          complete(this._updateGames(games)));
    }

    private _updateGames(games: api.game.Game[]): void {
      this._games = games
          .sort((a, b) => a.createdAt - b.createdAt)
          .map((game, i) => this._game(game, i));

      const game: model.game.Game  = this._games.sort((a, b) =>
          b.touchedAt - a.touchedAt)[0] || null;

      if (game !== null) {
        this.continueGame(game);
      } else {
        this._touch();
      }
    }

    private _open(game: model.game.Game): void {
      this._solver = new Solver(this._user, game,
          this, this._account, this._keyboard, this._explorer);

      this._solver.game.open();
      this._navigation.page = model.Page.GAME;
    }

    private _game(game: api.game.Game, index: number): model.game.Game {
      const [[width, height], cells, clue]: api.game.Puzzle = game.puzzle;

      const fieldCells: model.game.Cell[] = cells.toLowerCase().split('');
      const stateCells: model.game.Cell[] = cells.split('').map(
          (c) => c === c.toUpperCase() ? c : '');

      const field: model.game.Field =
          new model.game.Field(width, height, fieldCells, clue);

      const state: model.game.State =
          new model.game.State(field, stateCells);

      return new model.game.Game(game.id, state, index + 1, game.touchedAt);
    }
  }
}
