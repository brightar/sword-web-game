namespace sword.feat.role {

  import Feature = brightar.feat.Feature;
  import Callback = brightar.Callback;
  import Quest = sword.model.quest.Quest;
  import quests = sword.model.quest.quests;
  import Scenario = sword.model.quest.Scenario;
  import IStep = sword.model.quest.IStep;
  import State = sword.api.quest.State;
  import Balance = sword.api.account.Balance;

  export type Event = string;

  export class Explorer extends Feature {

    private _callbacks: { [key: string]: Callback } = {};
    private _quest: Quest = null;
    private _hidden: boolean = false;
    private _key: string = '-quest-hidden';
    private _user: User = null;

    constructor(private _id: api.quest.User) {
      super();

      this._key = _id + this._key;
      this._hidden = brightar.util.store.loadFlag(this._key);
    }

    get active(): boolean {
      return this._user !== null &&
          this._quest !== null &&
          this.step.isActive(this._user);
    }

    get hasReward(): boolean {
      return this.active && this._quest.rewardReady(this._user);
    }

    get hidden(): boolean {
      return this._hidden || !this.active;
    }

    set hidden(value: boolean) {
      this._touch(this._hidden = value);
      brightar.util.store.saveFlag(this._key, value);
    }

    get step(): IStep {
      return this._quest.step;
    }

    set user(user: User) {
      this._touch(this._user = user);
    }

    get user(): User {
      return this._user;
    }

    next(): void {
      if (this._quest.step.completed) {
        api.quest.complete(this._id, this._quest.name, (state: State, balance: Balance) => {
          this._check(state);
          this._user.account.balance = balance;
        });
        this._quest = null;
      } else if (this._quest.next(this._user)) {
        api.quest.step(this._id, this._quest.name);
      }

      this._touch();
    }

    wait(event: Event, callback: Callback): void {
      this._callbacks[event] = () => {
        this._callbacks[event] = undefined;
        callback();
      }
    }

    check(): void {
      api.quest.progress(this._id, (state: State) => {
        this._check(state);
      });
    }

    private _check(state: State): void {
      if (state !== null) {
        this._quest = quests.create(state);
        this._quest.step.start(this._user);
      }
      this._touch();
    }

    isExplored(events: Event[]): boolean {
      return !(events.length === 0) && events
          .map((event) => this._isExplored(event))
          .reduce((e, v) => e && v);
    }

    explore(event: Event): void {
      brightar.util.store.saveFlag(this._id + '-' + event, true);
      if (this._callbacks[event]) {
        this._callbacks[event]();
      }
    }

    private _isExplored(event: string): boolean {
      return brightar.util.store.loadFlag(this._id + '-' + event);
    }
  }
}
