namespace sword.view.window {

  import Widget = brightar.ui.Widget;

  import User = sword.model.User;
  import Page = sword.model.Page;
  import Position = sword.model.game.Position;
  import ShopBundle = sword.model.view.ShopBundle;
  import Shop = sword.feat.view.Shop;

  export class ShopWindow extends Widget<Shop> {
    constructor() {
      super(['shop'], ['click']);
    }

    _click(shop: Shop, target: d3.Selection<any>): boolean {
      if (target.classed('shop__close')) {
        shop.leave(false);

        return true;
      }

      if (target.classed('shop__row__buy-button')) {
        const bundle: ShopBundle = target.datum();
        shop.order(bundle);

        return true;
      }

      return false;
    }

    _init(node: d3.Selection<Shop>): void {
      node.highlighted(true)
          .element('header')
            .element('balance')
              .text((shop) => shop.account.letters)
              .parent()
            .element('caption')
              .text((shop) => shop.reason);

      node.element('logo').parent()
          .element('close').parent()
          .selectAll('.shop__row')
          .data((shop) => shop.bundles
              .filter((b: ShopBundle) => b.lettersAmount >= shop.minStars))
          .enter()
            .div('shop__row')
              .each(ShopWindow._row)
    }

    _update(shop: Shop, node: d3.Selection<Shop>): void {
      if (shop.hasFake()) {
        node.selectAll('.shop__close')
            .style('display', 'none')
      } else {
        node.selectAll('.shop__close')
            .style('display', null)
      }
    }

    _destroy(node: d3.Selection<Shop>): void {
      node.highlighted(false);
    }

    private static _row = function(): void {
      d3.select(this)
        .classed('shop__row--gift', (bundle) => bundle.discount === 100)
        .div('shop__row__pic')
          .style('background-image', (bundle) =>
              'url(' + bundle.picUrl + ')').parent()
          .div('shop__row__block')
            .div('shop__row__block__amount')
              .text((bundle) => bundle.lettersAmount).parent()
            .div('shop__row__block__bukovok').text((bundle) => numeric(bundle.lettersAmount, ['звездочка', 'звездочки', 'звездочек']))
            .parent()
            .div('shop__row__block__discount')
              .div('shop__row__block__discount__value')
                .text((bundle) => bundle.discount + '%')
              .parent()
              .div('shop__row__block__discount__text').text('скидка')
          .parent().parent().parent()
          .div('shop__row__price')
            .div('shop__row__price__value')
              .text((bundle) => bundle.price)
            .parent()
            .div('shop__row__price__text').text((bundle) => sword.api.platform.currency(bundle.price))
          .parent().parent()
          .div('shop__row__buy-button').text((bundle) => bundle.discount === 100 ? 'Забрать!' : 'Купить!');
    }
  }
}
