namespace sword.view.window {
  import Widget = brightar.ui.Widget;

  import Callback = brightar.Callback;
  import Explorer = sword.feat.role.Explorer;
  import IStep = sword.model.quest.IStep;

  export class QuestWindow extends Widget<Explorer> {
    constructor() {
      super(['quest'], ['click', 'mousedown']);
    }

    _init(node: d3.Selection<Explorer>): void {
      node.div('quest__hide');
      node.div('quest__body');
    }

    _mousedown(explorer: Explorer, child: d3.Selection<any>): boolean {
      d3.event.stopPropagation();

      return true;
    }

    _click(explorer: Explorer, child: d3.Selection<any>): boolean {
      if (child.classed('quest__body__button')) {
        explorer.next();

        return true;
      }

      if (child.classed('quest__hide')) {
        explorer.hidden = true;

        return true;
      }

      return false;
    }

    _update(explorer: Explorer, node: d3.Selection<Explorer>): void {
      const current: IStep = explorer.step;

      const body: d3.Selection<any> = node.select('.quest__body');
      const content: string = current.content(explorer.user);

      if (body.attr('data-content') !== content) {
        body.attr('data-content', content);
        body.html(content);
      }

      d3.selectAll('.highlighted').highlighted(false);
      // TODO: do not set class on root node!
      node.attr('class', explorer.step.style());
      node.highlighted(true);
      explorer.step.highlighted()
          .forEach((n) => d3.selectAll(n).highlighted(true));
    }

    _idle(explorer: Explorer, touch: Callback): void {
      explorer.idle(touch);
    }

    _destroy(node: d3.Selection<Explorer>): void {
      d3.selectAll('.highlighted').highlighted(false);
      node.highlighted(false);
    }
  }
}
