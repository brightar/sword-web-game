namespace sword.view {

  const _KEYBOARD_LAYOUT: { [code: number]: string } = {
    81: 'Й',
    87: 'Ц',
    69: 'У',
    82: 'К',
    84: 'Е',
    192: 'Е',
    89: 'Н',
    85: 'Г',
    73: 'Ш',
    79: 'Щ',
    80: 'З',
    219: 'Х',
    221: 'Ъ',
    65: 'Ф',
    83: 'Ы',
    68: 'В',
    70: 'А',
    71: 'П',
    72: 'Р',
    74: 'О',
    75: 'Л',
    76: 'Д',
    186: 'Ж',
    222: 'Э',
    90: 'Я',
    88: 'Ч',
    67: 'С',
    86: 'М',
    66: 'И',
    78: 'Т',
    77: 'Ь',
    188: 'Б',
    190: 'Ю',
    8: ' ',
    46: ' ',
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down'
  };

  const _ALPHABET: string = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
  const _VOWELS: string = "аеёиоуыэюя";
  const _CONSONANTS: string = "бвгджзйклмнпрстфхцчшщ";
  const _EXCLUDE: string = "ъь";
  const _HYPHENATE_RULES: RegExp[] = [
    '([' + _EXCLUDE + '])' +
    '([' + _ALPHABET + '][' + _ALPHABET + '])',

    '([' + _VOWELS + '])' +
    '([' + _VOWELS + '][' + _ALPHABET + '])',

    '([' + _VOWELS + '][' + _CONSONANTS + '])' +
    '([' + _CONSONANTS + '][' + _VOWELS + '])',

    '([' + _CONSONANTS + '][' + _VOWELS + '])' +
    '([' + _CONSONANTS + '][' + _VOWELS + '])',

    '([' + _VOWELS + '][' + _CONSONANTS + '])' +
    '([' + _CONSONANTS + '][' + _CONSONANTS + '][' + _VOWELS + '])',

    '([' + _VOWELS + '][' + _CONSONANTS + '][' + _CONSONANTS + '])' +
    '([' + _CONSONANTS + '][' + _CONSONANTS + '][' + _VOWELS + '])',

    '([' + _VOWELS + '][' + _CONSONANTS + '][' + _CONSONANTS + '])' +
    '([' + _CONSONANTS + '][' + _CONSONANTS + '][' + _CONSONANTS + ']' +
        '[' + _VOWELS + '])'
  ].map((rule) => new RegExp(rule, 'ig'));

  const _NUMERIC_CASES: number[] = [2, 0, 1, 1, 1, 2];

  export const STAR: string = '<span class="star"></span>';

  export const TIME_ZONE_OFFSET: number =
      new Date().getTimezoneOffset() * 60 * 1000;

  export function extractKey(event: KeyboardEvent): string {
    if (!event.ctrlKey && !event.metaKey && !event.altKey) {
      if (event.keyCode === 0) {
        if (/[а-яА-Я]/.test(event.key)) {
          event.preventDefault();

          return event.key.toUpperCase();
        }
      } else {
        event.preventDefault();

        return _KEYBOARD_LAYOUT[event.keyCode] || '';
      }
    }

    return '';
  }

  export function outerHeight(element: HTMLElement): number {
    var height = element.offsetHeight;
    var style = getComputedStyle(element);

    return height + parseInt(style.marginTop) +
        parseInt(style.marginBottom);
  }

  export function outerWidth(element: HTMLElement): number {
    var width = element.offsetWidth;
    var style = getComputedStyle(element);

    return width + parseInt(style.marginLeft) +
        parseInt(style.marginRight);
  }

  export function numeric(number: number,
                          titles: [string, string, string]): string {

    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 :
        _NUMERIC_CASES[(number % 10 < 5) ? number % 10 : 5]];
  }

  export function align(num: number): string {
    if (num < 10) return '0' + num;
    else return '' + num
  }

  export function hyphenate(text: string): string {
    for (var rule of _HYPHENATE_RULES) {
      while (true) {
        var update: string = text.replace(rule, '$1\u00AD$2');
        if (update === text) {
          break;
        }

        text = update;
      }
    }

    return text;
  }

  export function getStyleValue(node: d3.Selection<any>, prop: string): number {
    return Number(node.style(prop).replace(/[^\.\d]/ig, ''));
  }

  export function decreaseStyleValue(node: d3.Selection<any>,
                                     prop: string): void {

    node.style(prop, getStyleValue(node, prop) - 1 +
      node.style(prop).replace(/\d/ig, ''))
  }

  export function fitFontSize(node: HTMLElement, height: number) {
    if (node.scrollHeight > height) {
      decreaseStyleValue(d3.select(node), 'font-size');
      setTimeout(fitFontSize, 10, node, height)
    }
  }

  export function moreThan(attr: string, value: number): () => boolean {
    return function() {
      return view.getStyleValue(d3.select(this), attr) >= value;
    }
  }

  export function labelText(text: string): () => void {
    return () => {
      const label = d3.select(d3.event.target).parent().select('label');
      const value: string = (<HTMLTextAreaElement> d3.event.target).value;
      label.text(value === '' ? text : '');
    };
  }

  export function textValue(selection: d3.Selection<any>): string {
    return (<HTMLTextAreaElement> selection.node()).value;
  }


  export function date(time: number): Date {
    return new Date(time + TIME_ZONE_OFFSET);
  }
}
