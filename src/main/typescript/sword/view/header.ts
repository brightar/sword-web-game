namespace sword.view {

  import Widget = brightar.ui.Widget;

  import Page = sword.model.Page;
  import Position = sword.model.game.Position;
  import User = sword.model.User;
  import Callback = brightar.Callback;

  export class Header extends Widget<User> {
    constructor() {
      super(['header'], ['click']);
    }

    _click(user: User, target: d3.Selection<any>): boolean {
      if (target.classed('header__history__back')) {
        user.navigation.page = Page.HISTORY;

        return true;
      }

      if (target.classed('header__menu-do__quest')) {
        user.explorer.hidden = false;
        return true;
      }

      if (target.classed('header__menu-stars__stars')) {
        d3.event.stopPropagation();

        if (user.player.isHintReady === false) {
          user.tryOrder((ready) =>
              user.player.isHintReady = ready,
              'Не хватает звездочки', 1);
        } else {
          user.player.isHintReady = false;
        }

        return true;
      }

      if (target.classed('header__menu-stars__shop')) {
        user.shop.enter();

        return true;
      }

      if (target.classed('header__menu-stars__gift')) {
        user.gift.take();

        return true;
      }

      if (target.classed('header__next')) {
        user.tryOrder((bought) => {
          target.classed('header__next--pressed', bought);

          if (bought) {
            user.orderGame(() =>
                target.classed('header__next--pressed', false));
          }
        }, "Не хватает звездочек", api.platform.puzzlePrice());

        return true;
      }

      if (target.classed('header__menu-stars__invite') ||
          target.classed('invite__new')) {
        user.invite.send();

        return true;
      }

      if (target.classed('invite__avatar') && !target.classed('invite__new')) {
        user.invite.reward(target.datum().user);
        return true;
      }

      return false;
    }

    _init(node: d3.Selection<User>): void {
      const user: User = node.datum();
      node.element('logo')
            .style('transform', 'rotateZ(360deg)');

      node.element('history')
            .element('back')
              .tooltip('Предыдущие игры')
              .parent()
            .element('game-id');

      node.element('menu-do')
            .element('quest')
              .tooltip('Обучение')
              .element('status');

      node.element('menu-stars')
            .element('stars')
              .tooltip('Подсказка')
              .element('star')
                .style('transform', 'rotateZ(360deg)')
                .parent()
              .element('value')
                .parent()
              .element('actions')

          .parent('menu-stars')
            .element('shop')
              .tooltip('Магазин')
              .parent()
            .element('invite')
              .div('invite__icon').parent()
              .div('invite__tooltip').parent()
              .div('invite__hover')
                .tooltip('Пригласить друга')
                .text('+' + user.invite.amount())
                .span('star').parent()

          .parent('menu-stars')
            .element('gift')
              .tooltip('Подарок')
              .element('status')
                .span('minutes').parent()
                .span('colon anim-opacity')
                  .text(':')
                  .parent()
                .span('seconds').parent()
                .span('size').parent()
                .span('star')
            .parent('gift')
              .div('gift');

      node.element('next')
            .tooltip('Новая игра')
          .element('price')
            .element('value').parent()
            .element('star')
              .style('transform', 'rotateZ(3600deg)');

      const progress: d3.Selection<User> =
          node.select('.header__menu-stars__stars__actions');

      progress.attr('data-max-height',
          getStyleValue(progress, 'height'));
    }

    _update(user: User, node: d3.Selection<User>): void {
      node.classed('header--history', user.navigation.page === Page.HISTORY)
          .classed('header--game', user.navigation.page === Page.GAME)
          .classed('header--active-quest', user.explorer.active);

      node.select('.header__menu-do__quest')
          .classed('header__menu-do__quest--reward', user.explorer.hasReward);

      this._updateGift(node);
      this._updateActions(node, user.tillNewLetter());
      this._updateLetters(node, user.account.letters);
      this._updateInvited(node, user.invite.accepted);

      node.select('.header__history__game-id').text(
          user.player.solver !== null &&
              user.navigation.page == Page.GAME ?
                  '№' + user.player.solver.game.index : '');

      node.select('.header__next__price__value')
          .text(api.platform.puzzlePrice());
    }

    _idle(user: User, touch: Callback): void {
      user.idle(touch);
    }

    private _updateGift(root: d3.Selection<User>): void {
      const user = root.datum();
      const time = date(user.gift.timeRemain);
      const size = user.gift.size;
      root.select('.header__menu-stars__gift').classed('wait-gift', size <= 0);
      if (user.gift.timeRemain >= 0) {
        root.select('.minutes').text(align(time.getMinutes()));
        root.select('.seconds').text(align(time.getSeconds()));
      }
      root.select('.size').text('+' + size);

      if (size <= 0 && user.gift.timeRemain > 0) {
        setTimeout(() => {
          this._updateGift(root);
        }, 1000);
      }
    }

    private _updateActions(root: d3.Selection<User>, remain: number): void {
      const node: d3.Selection<any> =
          root.select('.header__menu-stars__stars__actions');

      const max: number = Number(node.attr('data-max-height'));
      const current: number = remain * max;
      const progress: number = current === 0 ? max : current;

      if (Math.abs(current - getStyleValue(node, 'border-bottom-width')) > 1) {
        node.transition('actions')
            .ease('bounce')
            .delay(100)
            .duration(500)
            .style('border-bottom-width', progress + 'px')
            .each('end', () => {
              if (current !== progress) {
                node.transition('actions')
                    .duration(300)
                    .style('opacity', 0).each('end', () => {
                  node.style('border-bottom-width', current)
                      .style('opacity', 1);
                });
              }
            });
      }
    }

    private _updateLetters(node: d3.Selection<User>, count: number): void {
      const lettersNode: d3.Selection<any> =
          node.select('.header__menu-stars__stars__value');

      const lettersBefore: number = Number(lettersNode.text());

      if (count !== lettersBefore) {
        d3.select('.header__menu-stars__stars__star')
            .transition()
            .style('transform', 'rotateZ(' +
                ((count < lettersBefore) ? '-' : '') + '360deg)')
            .duration(500);

        if (count > lettersBefore) {
          brightar.util.audio.play('money');
        }
      }

      lettersNode.text(count);
    }

    private _updateInvited(node: d3.Selection<User>,
                           accepted: api.platform.Profile[]): void {
      node.select('.header__menu-stars__invite')
          .classed('invite--has-accepted', accepted.length > 0);

      const tooltip = node.select('.invite__tooltip')
          .style('display', accepted.length > 0 ? '' : 'none')
          .style('width', (accepted.length === 1 ? 104 : 154) + 'px');

      const update = tooltip.selectAll('.invite__avatar').data(accepted);
      update.enter().div('invite__avatar');
      update.exit().remove();
      update.style('background-image', (f) => 'url(' + f.photo + ')')
          .classed('invite__new', false);

      tooltip.div('invite__avatar invite__new');
    }
  }
}
