namespace sword.view.page {

  import Control = brightar.ui.Widget;
  import User = sword.model.User;
  import Game = sword.model.game.Game;
  import Field = sword.model.game.Field;
  import State = sword.model.game.State;
  import Position = sword.model.game.Position;
  import Callback = brightar.Callback;
  import Solver = sword.feat.role.Solver;

  export class GamePage extends Control<Solver> {
    constructor() {
      super(['page', 'page--game', 'grid'], ['click', 'mousedown'])
    }

    _mousedown(solver: Solver, target: d3.Selection<any>): boolean {
      if (target.classed('page')) {
        d3.event.stopPropagation();
        solver.focus();

        return true;
      }

      return false;
    }

    _click(solver: Solver, target: d3.Selection<any>): boolean {
      if (target.classed('grid__cell--clue') && !solver.player.isHintReady) {
        solver.selectWord(target.datum());

        return true;
      }

      if (target.classed('grid__cell--letter')) {
        if (solver.player.isHintReady) {
          const position = target.datum();
          if (solver.game.state.isAvailable(position)) {
            solver.useHint(target.datum());

            brightar.util.audio.play('wand');
          }
        } else {
          solver.selectLetter(target.datum());
        }

        return true;
      }

      return false;
    }

    _init(node: d3.Selection<Solver>): void {
      const prototype: d3.Selection<any> = node.div('grid__cell');

      node.attr('cell-width', outerWidth(<HTMLElement> prototype.node()))
          .attr('cell-height', outerHeight(<HTMLElement> prototype.node()))
          .style('position', 'relative');

      prototype.remove();
    }

    _update(solver: Solver, node: d3.Selection<Solver>): void {
      const game: Game = solver.game;
      const state: State = game.state;
      const field: Field = game.field;

      const width: number = Number(node.attr('cell-width'));
      const height: number = Number(node.attr('cell-height'));

      node.style('width',  field.getWidth() * width + 'px')
          .style('height', field.getHeight() * height + 'px');

      const letters: d3.selection.Update<Position> =
          node.selectAll('.grid__cell.grid__cell--letter')
              .data(field.getLetters(), (pos, _) => pos.toString());

      letters.enter()
          .div('grid__cell grid__cell--letter')
            .style('position', 'absolute')
            .style('top', (pos) => height * field.getY(pos) + 'px')
            .style('left', (pos) => width * field.getX(pos) + 'px')
            .div('grid__cell-content');

      letters
          .classed('grid__cell--current-letter', (pos) => game.isCurrentLetter(pos))
          .classed('grid__cell--current-word', (pos) => game.isCurrentWord(pos))
          .classed('grid__cell--victory', () => game.isWon)
          .classed('grid__cell--disabled-letter', (pos) => state.isOpened(pos))
          .select('.grid__cell-content')
            .text((pos) => state.getChar(pos));

        const arrows: d3.selection.Update<string> = letters
            .selectAll('.grid__cell-background')
            .data((pos) => field.getLetterArrows(pos), (arrow) => arrow);

        arrows.enter().append('div');
        arrows.exit().remove();
        arrows.attr('class', (arrow) =>
            'grid__cell-background grid__cell-background--' + arrow);

      letters.exit()
          .remove();

      const clues: d3.selection.Update<Position> =
          node.selectAll('.grid__cell.grid__cell--clue')
              .data(field.getClues(), (pos, _) => pos.toString());

      clues.enter()
          .div('grid__cell grid__cell--clue')
            .style('position', 'absolute')
            .style('top', (pos) => height * field.getY(pos) + 'px')
            .style('left', (pos) => width * field.getX(pos) + 'px')
            .div('grid__cell-content');

      clues.exit()
          .remove();

      clues
          .classed('grid__cell--not-solved', (pos) => !state.solved(pos))
          .classed('grid__cell--victory', () => game.isWon)
          .classed('grid__cell--current-word', (pos) => game.isCurrentWord(pos))
          .select('.grid__cell-content')
            .text((pos) => hyphenate(field.getWording(pos)))
            .each(function() { fitFontSize(this, height) });
    }

    _idle(solver: Solver, touch: Callback): void {
      solver.idle(touch)
    }
  }
}
