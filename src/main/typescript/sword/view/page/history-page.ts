namespace sword.view.page {

  import Widget = brightar.ui.Widget;

  import User = sword.model.User;

  import Field = sword.model.game.Field;
  import State = sword.model.game.State;
  import Game = sword.model.game.Game;

  import platform = sword.api.platform;
  import Callback = brightar.Callback;

  export class HistoryPage extends Widget<Player> {
    constructor() {
      super(['page', 'page--history'], ['click']);
    }

    _click(player: Player, child: d3.Selection<any>): boolean {
      if (child.classed('grid-preview')) {
        player.continueGame(child.datum(), true);

        return true;
      }

      return false;
    }

    _update(player: Player, node: d3.Selection<Player>): void {
      const tiles: d3.selection.Update<Game> = node
          .selectAll('.grid-preview')
          .data(player.allGames.sort((a, b) => b.touchedAt - a.touchedAt));

      tiles.enter()
          .div('grid-preview')
            .each(HistoryPage._gamePreviewInit);

      tiles
          .classed('grid-preview--big', (_, i) => i < 2)
          .each(HistoryPage._gamePreviewUpdate);
    }

    _idle(player: Player, touch: Callback): void {
      player.idle(touch)
    }

    private static _gamePreviewUpdate = function(game: Game): void {
      const node: d3.Selection<Game> = d3.select(this);
      const state: State = game.state;

      node.selectAll('.grid-element')
          .modify('victory', game.isWon)
          .modify('disabled-' + Field.LETTER,
              (pos) => state.isOpened(pos));
    };

    private static _gamePreviewInit = function(game: Game): void {
      const node: d3.Selection<Game> = d3.select(this);
      const field: Field = game.field;

      node.element('header')
          .text('Сканворд №' + game.index)
        .parent()
          .element('desc')
          .text('Решено ' + Math.round(100 * game.solveRatio) + '%')
        .parent()
          .selectAll('.grid-element')
          .data(field.getGrid())
          .enter()
            .append('div')
            .attr('class', (pos) =>
                'grid-element grid-element--' + field.getCellType(pos));
    }
  }
}
