namespace sword.view.page {

  import Widget = brightar.ui.Widget;

  export class PreloaderPage extends Widget<User> {
    private _TIMEOUT: number = 30000;

    constructor() {
      super(['preloader']);
    }

    _init(node: d3.Selection<User>): void {
      node.append('h1').text('Игра Слов').parent()
          .append('h2').text('Сканворды').parent()
          .element('logo').classed('anim-rotate360', true);

      setTimeout(() => {
        node.selectAll('*').remove();
        node.add('p', 'not-working')
            .append('h3').text('Игра временно недоступна.').parent()
            .append('h4').text('Попробуйте еще через несколько минут.');
      }, this._TIMEOUT);
    }
  }
}
