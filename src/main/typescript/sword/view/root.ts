namespace sword.view {

  import IState = brightar.service.IState;
  import Expand = brightar.service.Expand;
  import Widget = brightar.ui.Widget;

  import User = sword.model.User;
  import Game = sword.model.game.Game;
  import Page = sword.model.Page;

  import GamePage = sword.view.page.GamePage;
  import HistoryPage = sword.view.page.HistoryPage;
  import PreloaderPage = sword.view.page.PreloaderPage;

  import ShopWindow = sword.view.window.ShopWindow;
  import Callback = brightar.Callback;
  import Solver = sword.feat.role.Solver;
  import Player = sword.feat.role.Player;
  import Shop = sword.feat.view.Shop;
  import QuestWindow = sword.view.window.QuestWindow;

  export class Root extends Widget<User> {
    private _historyPageService: IState<Player> = null;
    private _gamePageService: IState<Solver> = null;
    private _shopWindowService: IState<Shop> = null;
    private _questWindowService: IState<Explorer> = null;
    private _preloaderService: IState<User> = null;

    constructor(private _userService: IState<User>) {
      super(['application'], ['click', 'keydown', 'keypress', 'mousedown']);

      this._historyPageService = new Expand(_userService, (user) =>
          user.navigation.page === Page.HISTORY ? user.player : null);

      this._gamePageService = new Expand(_userService, (user) =>
          user.navigation.page === Page.GAME ? user.player.solver : null);

      this._preloaderService = new Expand(_userService, (user) =>
          user.navigation.page === Page.PRELOADER ? user : null);

      this._shopWindowService = new Expand(_userService, (user) =>
          user.shop.isOpened ? user.shop : null);

      this._questWindowService = new Expand(_userService, (user) =>
          !user.explorer.hidden ? user.explorer : null);
    }

    _init(node: d3.Selection<User>): void {
      brightar.ui.widget(node.div('header-container'),
          new Header(), this._userService);

      brightar.ui.widget(node, new ShopWindow(), this._shopWindowService);
      brightar.ui.widget(node, new QuestWindow(), this._questWindowService);
      brightar.ui.widget(node, new GamePage(), this._gamePageService);
      brightar.ui.widget(node, new HistoryPage(), this._historyPageService);
      brightar.ui.widget(node, new PreloaderPage(), this._preloaderService);
    }

    _click(user: User, child: d3.Selection<any>): boolean {
      if (user.player.isHintReady) {
        user.player.isHintReady = false;
      }

      return true;
    }

    _keydown(user: User, child: d3.Selection<any>): boolean {
      const key: string = extractKey(d3.event);

      if (key !== '') {
        user.keyboard.applyKey(key);
      }

      return true;
    }

    _keypress(user: User, child: d3.Selection<any>): boolean {
      user.keyboard.applyKey(String.fromCharCode(d3.event.charCode));

      return true;
    }

    _mousedown(user: User, child: d3.Selection<any>): boolean {
      if (!child.classed('overlay')) {
        user.keyboard.blur();

        return true;
      }

      return false;
    }

    _update(user: User, node: d3.Selection<User>): void {
      node.classed('hinted', user.player.isHintReady);
    }

    _idle(user: User, touch: Callback): void {
      user.idle(touch);
    }
  }
}
