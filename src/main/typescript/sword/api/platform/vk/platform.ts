namespace sword.api.platform.vk {

  import Hash = brightar.util.Hash;
  import decodeUrlParams = brightar.util.decodeUrlParams;

  export class Platform implements IPlatform {
    private static _nop: Cancel = () => {};
    private _complete: Cancel = Platform._nop;
    private _cancel: Cancel = Platform._nop;
    private _ID: string = 'vk';

    init(complete: Init): void {
      const params = decodeUrlParams();
      const user = JSON.parse(params['api_result'])['response'][0];

      VK.addCallback('onOrderSuccess', () => this._complete());
      VK.addCallback('onOrderFail', () => this._cancel());
      VK.addCallback('onOrderCancel', () => this._cancel());

      complete(this._ID + user['uid'], {
        'referer_id': this._ID + params['user_id'] || params['group_id'],
        'referer_type': params['referrer'],
        'sex': user['sex'] === 2 ? 'M' : 'F',
        'age': '0',
        'profile_link': 'https://vk.com/id' + user['link'],
        'location': '',
        'user': this._ID + user['uid']
      });
    }

    buy(service: number,
        name: string,
        message: string,
        price: number,
        complete: Cancel,
        cancel: Cancel): void {
      const params = {
        type: 'item',
        item: 'stars_' + service + '_' + price
      };

      this._complete = this._callback(complete);
      this._cancel = this._callback(cancel);
      VK.callMethod('showOrderBox', params);
    }

    currency(count: number): string {
      return view.numeric(count, ['голос', 'голоса', 'голосов']);
    }

    populate(users: string[], complete: Populate): void {
      VK.api('users.get', {
        'user_ids': users.map(this._id).join(','),
        'fields': 'photo_200',
        'name_case': 'nom'
      }, (data: { response: Hash[] }) => {
        complete(data['response'].map((record: Hash) => {
          return {
            user: this._ID + record['id'],
            firstName: record['first_name'],
            lastName: record['last_name'],
            photo: record['photo_200']
          };
        }));
      });
    }

    invite(message: string, complete: Invite): void {
      VK.callMethod('showInviteBox');
    }

    invitedBy(): string {
      const params = decodeUrlParams();
      if (params['user_id'] !== params['viewer_id']) {
        return this._ID + params['user_id'];
      }
      return '';
    }

    private _callback(callback: Cancel): Cancel {
      return () => {
        this._clear();
        callback();
      }
    }

    private _clear(): void {
      this._cancel = Platform._nop;
      this._complete = Platform._nop;
    }

    private _id(id: string): string {
      return id.replace('vk', '');
    }
  }
}
