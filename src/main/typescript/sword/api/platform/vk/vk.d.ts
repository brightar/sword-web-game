declare namespace VK {
  import Cancel = sword.api.Cancel;

  export function api(method: string, params: Object, callback: (data: Object) => void): void;
  export function callMethod(method: string, ...params: any[]): void;
  export function addCallback(event: string, callback: Cancel): void;
}
