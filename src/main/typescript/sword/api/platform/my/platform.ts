namespace sword.api.platform.my {

  import Hash = brightar.util.Hash;
  import decodeUrlParams = brightar.util.decodeUrlParams;

  export class Platform implements IPlatform {

    private _paymentListener: number = -1;

    init(complete: Init): void {
      const PROD_ID = '8d672c8e0b12d69e7d3468833847de04';
      const STAGING_ID = 'da5a08c2cd321a53e3379a124e6d4974';
      const ID = location.href.indexOf('https://staging.devlpr.tk') === 0 ?
          STAGING_ID : PROD_ID;
      mailru.app.init(ID);
      mailru.common.users.getInfo((users) => {
        const user: Hash = users.shift();

        complete(user['uid'], {
          'referer_id': user['referer_id'],
          'referer_type': user['referer_type'],
          'sex': user['sex'] === 0 ? 'M' : 'F',
          'age': user['age'],
          'profile_link': user['link'],
          'location': user['location'],
          'user': user['uid']
        });
      });
    }

    buy(service: number,
        name: string,
        message: string,
        price: number,
        complete: Cancel,
        cancel: Cancel): void {

      const payment: string = mailru.app.events.incomingPayment;
      const dialog: string = mailru.app.events.paymentDialogStatus;

      if (this._paymentListener === -1) {
        this._paymentListener = mailru.events.listen(payment, (event) => {
          mailru.events.remove(this._paymentListener);
          this._paymentListener = -1;

          if (event.status === 'success') {
            complete();
          } else {
            cancel();
          }
        });

        var dialogListener = mailru.events.listen(dialog, (event) => {
          if (event.status === 'opened') {
            mailru.events.remove(dialogListener);

            d3.select(document.body).once('mousemove', () => {
              if (this._paymentListener !== -1) {
                mailru.events.remove(this._paymentListener);
                this._paymentListener = -1;

                cancel();
              }
            });
          }
        });

        mailru.app.payments.showDialog({
          service_id: service,
          service_name: name,
          mailiki_price: price
        });
      }
    }

    currency(count: number): string {
      return view.numeric(count, ['мейлик', 'мейлика', 'мейликов']);
    }

    populate(users: User[], complete: Populate): void {
      mailru.common.users.getInfo((users) => complete(users.map((record) => {
        return {
          user: record['uid'],
          firstName: record['first_name'],
          lastName: record['last_name'],
          photo: record['pic'],
        }
      })), users);
    }

    invite(message: string, complete: Invite): void {
      mailru.events.listen(mailru.app.events.friendsInvitation, function(event) {
        if (event['status'] === 'closed') {
          complete(event.data);
        }
      });
      mailru.app.friends.invite();
    }

    invitedBy(): string {
      const params = decodeUrlParams();
      if (params['referer_type'] === 'invitation') {
        return params['referer_id'];
      }

      return '';
    }
  }
}
