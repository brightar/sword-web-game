declare namespace mailru.events {
  export type Event = {status: string, data: string[], friends: string[]};
  export function listen(event: string, callback: (any: Event) => void): number;
  export function remove(listener: number): void;
}

declare namespace mailru.app.events {
  export const incomingPayment: string;
  export const paymentDialogStatus: string;
  export const friendsInvitation: string;
  export const friendsRequest: string;
}

declare namespace mailru.app {
  export function init(app: string): void;
}

declare namespace mailru.app.payments {
  export function showDialog(options: {
    service_id: number,
    service_name: string,
    mailiki_price: number
  }): void;
}

declare namespace mailru.app.friends {
  export function invite(): void;
  export function request(params: {image_url: string, text: string}): void;
}

declare namespace mailru.common.users {
  export function getInfo(complete: (user: Hash[]) => void,
                          id?: string[]): void;
}

declare namespace mailru.common.friends {
  export function getExtended(callback: (users: Hash[]) => void): void;
}
