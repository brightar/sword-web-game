namespace sword.api.platform.local {

  import util = brightar.util;

  export class Platform implements IPlatform {

    init(complete: Init): void {
      complete(location.search.substring(1), {});
    }

    buy(service: number,
        name: string,
        message: string,
        price: number,
        complete: Cancel,
        cancel: Cancel): void {
      cancel();
    }

    currency(count: number): string {
      return '';
    }

    populate(users: User[], complete: Populate): void {
      complete(users.map((user) => {
        return {
          user: user,
          firstName: user,
          lastName: 'Testovich',
          photo: 'https://d13yacurqjgara.cloudfront.net/users/286922/screenshots/1710022/grandma_1x.jpg'
        };
      }));
    }

    invite(message: string, complete: Invite): void {
      complete(['invited' + util.uid()]);
    }

    invitedBy(): string {
      return 'test1';
    }
  }
}
