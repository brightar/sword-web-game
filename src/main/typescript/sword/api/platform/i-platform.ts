namespace sword.api.platform {

  import Hash = brightar.util.Hash;




  export interface IPlatform {
    buy(service: number,
        name: string,
        message: string,
        price: number,
        complete: Cancel,
        cancel: Cancel): void

    init(complete: Init): void
    currency(count: number): string

    populate(users: User[], complete: Populate): void
    invite(message: string, complete: Invite): void
    invitedBy(): string
  }
}
