namespace sword.api.platform {

  import Hash = brightar.util.Hash;
  import ShopBundle = sword.model.view.ShopBundle;

  export type User = string
  export type Profile = {
    user: User,
    firstName: string,
    lastName: string,
    photo: string
  }

  export type Init = (user: User, data: Hash) => void
  export type Populate = (data: Profile[]) => void
  export type Invite = (users: User[]) => void

  export const PROFILE_DUMMY: Profile = {
    user: '',
    firstName: 'Имя',
    lastName: 'Фамилия',
    photo: ''
  };

  export function populate(users: User[], complete: Populate): void {
    return _platform.populate(users, complete);
  }

  export function invite(message: string, complete: Invite): void {
    return _platform.invite(message, complete);
  }

  export function invitedBy(): string {
    return _platform.invitedBy();
  }

  // -------------------------------------------------------------------------

  export function init(platform: string,
                       options: Hash,
                       complete: Init): void {

    if (_platform === null) {

      _PUZZLE_PRICE = options['puzzlePrice'];
      _DAILY_GIFT_SIZE = options['dailyGiftSize'];
      const exchangeRate = options['exchangeRate'][platform];
      _BUNDLES = options['bundles'].map((obj: Hash) => new ShopBundle(
          obj['serviceId'], obj['lettersAmount'],
          Math.floor(obj['price'] / exchangeRate), obj['discount'],
          'shop/bundle' + obj['serviceId'] + '.svg'));
      _ACTIONS_FOR_STAR = options['actionsRate'];
      _STARS_FOR_FRIEND = Math.floor(
          options['actions']['friendInvite'] / _ACTIONS_FOR_STAR);
      _QUESTS = options['quests'];

      _platform = _platformFactory(platform);
      _platform.init(complete)
    }
  }

  export function buyLetters(bundle: ShopBundle,
                             complete: Cancel,
                             cancel: Cancel): void {
    if (_platform !== null) {
      const name: string = bundle.lettersAmount + view.numeric(
          bundle.lettersAmount, [' зведочка', ' звездочки', ' звездочк']);

      const message: string = 'Зведочки нужны для открытия букв и новых ' +
          'сканвордов! Ваша скидка — ' + bundle.discount + '%';

      _platform.buy(bundle.serviceId, name, message,
          bundle.price, complete, cancel);
    } else {
      cancel()
    }
  }

  export function puzzlePrice(): number {
    return _PUZZLE_PRICE;
  }

  export function dailyGiftSize(): number {
    return _DAILY_GIFT_SIZE;
  }

  export function currency(count: number): string {
    return _platform.currency(count);
  }


  export function bundles(): ShopBundle[] {
    return _BUNDLES;
  }

  export function actionsForStar(): number {
    return _ACTIONS_FOR_STAR;
  }

  export function starsForFriend(): number {
    return _STARS_FOR_FRIEND;
  }

  export function questReward(name: string): number {
    return _QUESTS.filter((q) => q['name'] === name)
                  .map((q) => q['prize'])[0] || 0;
  }

  var _platform: IPlatform = null;
  var _PUZZLE_PRICE: number = 0;
  var _BUNDLES: ShopBundle[] = [];
  var _ACTIONS_FOR_STAR: number = 0;
  var _STARS_FOR_FRIEND: number = 0;
  var _QUESTS: Hash[] = [];
  var _DAILY_GIFT_SIZE: number = 0;

  function _platformFactory(name: string): IPlatform {
    switch (name) {
      case 'my': return new my.Platform();
      case 'ok': return new ok.Platform();
      case 'vk': return new vk.Platform();
    }

    return new local.Platform();
  }
}
