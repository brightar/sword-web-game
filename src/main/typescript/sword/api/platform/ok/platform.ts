function API_callback(method: string, result: string, data: string): void {
  if (method === 'showPayment') {
    sword.api.platform.ok.completePayment();
  }

  if (method === 'showNotification' || method === 'showInvite') {
    if (result !== 'cancel') sword.api.platform.ok.completeSend(data.split(','));
    else sword.api.platform.ok.cancelSend();
  }
}

namespace sword.api.platform.ok {

  import Hash = brightar.util.Hash;
  import decodeUrlParams = brightar.util.decodeUrlParams;

  var _paymentListener: [Cancel, Cancel] = null;
  var _inviteListener: Invite = null;

  export function completePayment(): void {
    if (_paymentListener !== null) {
      _paymentListener[0]();
      _paymentListener = null;
    }
  }

  export function cancelPayment(): void {
    if (_paymentListener !== null) {
      _paymentListener[1]();
      _paymentListener = null;
    }
  }
  export function completeSend(ids: string[]): void {
    if (_inviteListener !== null) {
      _inviteListener(ids);
      _inviteListener = null;
    }
  }

  export function cancelSend(): void {
    if (_inviteListener !== null) {
      _inviteListener([]);
      _inviteListener = null;
    }
  }

  export class Platform implements IPlatform {

    private _id: string = '';

    init(complete: Init): void {

      const rParams = FAPI.Util.getRequestParameters();
      FAPI.init(rParams['api_server'], rParams['apiconnection'],
          function() {
            const params: { [key: string]: string } =
                FAPI.Util.getRequestParameters();

            this._id = params['logged_user_id'];

            FAPI.Client.call({
              "method": "users.getInfo", "uids": params['logged_user_id'],
              "fields": ["first_name", "last_name", "age", "gender", "url_profile"]
            }, function(err: string, answers: { [key: string]: string }[]) {
              if (err === 'ok') {
                const answer = answers.shift();

                complete(params['logged_user_id'], {
                  'sex': answer['gender'] === 'male' ? 'M' : 'F',
                  'age': answer['age'],
                  'profile_link': answer['url_profile'],
                  'location': params['ip_geo_location'],
                  'user': params['logged_user_id']
                });
              }
            });

          }, function(error) {
            console.error(error);
          }
      );
    }

    buy(service: number,
        name: string,
        message: string,
        price: number,
        complete: Cancel,
        cancel: Cancel): void {
      if (_paymentListener === null) {
        _paymentListener = [complete, cancel];

        FAPI.UI.showPayment(name, message, service.toString(), price,
            null, null, 'ok', 'true');

        d3.select(document.body)
          .once('mousemove', cancelPayment);
      }
    }

    currency(count: number): string {
      return 'ОК';
    }

    populate(users: string[], complete: Populate): void {
      FAPI.Client.call({
        'method': 'users.getInfo',
        'uids': users,
        'fields': 'uid,first_name,last_name,pic_2,pic_1'
      }, (_: any, data: { [key: string]: string }[]) => {
        complete(data.map((record) => { return {
          user: record['uid'],
          firstName: record['first_name'],
          lastName: record['last_name'],
          photo: record['pic_2']
        }}))
      });
    }

    invite(message: string, complete: Invite): void {
      _inviteListener = complete;
      FAPI.UI.showInvite(message, "invited_by=" + this._id);
    }

    invitedBy(): string {
      const params = decodeUrlParams();
      if (params['refplace'] === 'friend_invitation') {
        return params['referer'];
      }
      return '';
    }
  }
}
