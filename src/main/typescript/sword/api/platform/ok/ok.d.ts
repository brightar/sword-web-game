declare namespace FAPI {
  export function init(apiServer: string, apiConnection: string,
                       complete: () => void,
                       cancel: (error: string) => void): void;
}

declare namespace FAPI.Util {
  export function getRequestParameters(): { [key: string]: string }
}

declare namespace FAPI.UI {
  export function showPayment(name: string,
                              desc: string,
                              service: string,
                              price: number,
                              opts: any,
                              attrs: any,
                              currency: string,
                              callback: string): void

  export function showNotification(
      text: string, args: string, ids?: string ): void

  export function showInvite(
      text: string, args: string, ids?: string ): void
}

declare namespace FAPI.Client {
  export function call(params: Object,
                       callback: (err: string, answers: any) => void): void;
}
