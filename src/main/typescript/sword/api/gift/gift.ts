namespace sword.api.gift {

  export type User = string;
  export type Size = number;
  export type Time = number;
  export type Gift = [Size, Time];

  export type Check = (gift: Gift) => void
  export type Take = (balance: account.Balance, gift: Gift) => void

  export function check(user: User, complete: Check): void {
    request<Gift>('/gift/check', { 'user': user }, complete);
  }

  export function take(user: User, complete: Take): void {
    requestPair<account.Balance, Gift>('/gift/take', {
      'user': user
    }, complete);
  }
}
