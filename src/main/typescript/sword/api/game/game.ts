namespace sword.api.game {

  export type Id = string;
  export type Clue = { [position: number]: string };
  export type Size = [number, number];
  export type Grid = string;
  export type Puzzle = [Size, Grid, Clue];

  export type User = string;

  export type RawGame = {
    id: Id,
    puzzle: string,
    createdAt: number,
    touchedAt: number
  }

  export type Game = {
    id: Id,
    puzzle: Puzzle,
    createdAt: number,
    touchedAt: number
  }

  export type Load = (games: Game[]) => void
  export type Save = (balance: account.Balance) => void

  export function load(user: User, complete: Load): void {
    request<RawGame[]>('/game/load', { 'user': user }, (result) =>
        complete(result.map(bake)));
  }

  export function save(user: User,
                       game: Id,
                       state: Puzzle,
                       words: number,
                       isWon: boolean,
                       complete: Save): void {
    request<account.Balance>('/game/save', {
      'user': user,
      'game': game,
      'words': words,
      'state': JSON.stringify(state),
      'isWon': isWon
    }, complete);
  }

  export function touch(game: Id): void {
    request<void>('/game/touch', { 'game': game }, skip);
  }

  export function bake(raw: RawGame): Game {
    return {
      id: raw.id,
      puzzle: JSON.parse(raw.puzzle),
      createdAt: raw.createdAt,
      touchedAt: raw.touchedAt
    }
  }
}
