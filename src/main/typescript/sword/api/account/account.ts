namespace sword.api.account {

  export type User = string;
  export type Balance = [number, number];

  export type Check = (balance: Balance) => void
  export type Payment = (balance: Balance) => void

  export function check(user: User, complete: Check): void {
    request<Balance>('/account/balance', { 'user': user }, complete);
  }

  export function puzzlePayment(user: User,
                                invoice: puzzle.Invoice,
                                complete: Payment): void {

    request<Balance>('/account/payment/puzzle', {
      'user': user,
      'invoice': invoice
    }, complete);
  }

  export function letterPayment(user: User, complete: Payment): void {
    request<Balance>('/account/payment/letter', { 'user': user, 'count': 1 }, complete);
  }
}
