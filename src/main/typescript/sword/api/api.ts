namespace sword.api {

  export type Complete<T> = (result: T) => void;
  export type CompletePair<A, B> = (a: A, b: B) => void;
  export type Cancel = () => void;

  export function skip(): void {}

  export function request<T>(method: string,
                             arguments: Object,
                             complete: Complete<T>,
                             cancel: Cancel = skip): void {

    d3.json(method).post(JSON.stringify(arguments), (error, result) => {
      if (error !== null) cancel();
      else complete(<T> result);
    });
  }

  export function requestPair<A, B>(method: string,
                                    arguments: Object,
                                    complete: CompletePair<A, B>,
                                    cancel: Cancel = skip): void {

    d3.json(method).post(JSON.stringify(arguments), (error, result) => {
      if (error !== null) cancel();
      else complete(<A> result[0], <B> result[1]);
    });
  }
}
