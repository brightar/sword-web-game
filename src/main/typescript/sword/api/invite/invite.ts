namespace sword.api.invite {

  export type User = string

  export type Check = (friends: User[]) => void
  export type Reward = (balance: account.Balance) => void

  export function invite(player: User, friends: User[]): void {
    if (friends.length > 0) {
      request<void>('/invite/invite', {
        'player': player,
        'friends': friends
      }, skip);
    }
  }

  export function check(player: User, complete: Check): void {
    request<User[]>('/invite/check', { 'player': player }, complete);
  }

  export function reward(player: User, friend: User, complete: Reward): void {
    request<account.Balance>('/invite/reward', {
      'player': player,
      'friend': friend
    }, complete);
  }

  export function succeed(friend: User): void {
    request<void>('/invite/succeed', { 'friend': friend }, skip);
  }

  export function invitedBy(player: User, friend: User): void {
    request<void>('/invite/invitedBy', {
      'player': player,
      'friend': friend
    }, skip);
  }
}
