namespace sword.api.quest {

  export type User = string
  export type Step = number
  export type Quest = string
  export type State = [Quest, Step]

  export type Progress = (state: State) => void
  export type Complete = (state: State, balance: account.Balance) => void

  export function progress(user: User, complete: Progress): void {
    request<State>('/quest/progress', {
      'user': user
    }, complete);
  }

  export function step(user: User, quest: Quest): void {
    request<void>('/quest/step', {
      'user': user,
      'quest': quest
    }, skip);
  }

  export function complete(user: User, quest: Quest, complete: Complete): void {
    requestPair<State, account.Balance>('/quest/complete', {
      'user': user,
      'quest': quest
    }, complete);
  }
}
