namespace sword.api.puzzle {

  export type User = string
  export type Invoice = string

  export type Order = (invoice: Invoice) => void
  export type Commit = (result: game.Game) => void

  export function makeOrder(user: User, complete: Order): void {
    request<Invoice>('/puzzle/order/make', { 'user': user }, complete);
  }

  export function commitOrder(user: User,
                              invoice: Invoice,
                              complete: Commit): void {

    request<game.RawGame>('/puzzle/order/commit', {
      'user': user,
      'invoice': invoice
    }, (result) => complete(game.bake(result)));
  }
}