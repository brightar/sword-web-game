namespace sword.model {

  import log = brightar.log;
  import store = brightar.util.store;

  import Page = sword.model.Page;
  import Game = sword.model.game.Game;
  import ShopBundle = sword.model.view.ShopBundle;
  import Gift = sword.feat.Gift;
  import Feature = brightar.feat.Feature;
  import Account = sword.feat.Account;
  import Callback = brightar.Callback;
  import Player = sword.feat.role.Player;
  import Explorer = sword.feat.role.Explorer;
  import Keyboard = sword.feat.view.Keyboard;
  import Navigation = sword.feat.view.Navigation;
  import Shop = sword.feat.view.Shop;
  import ShopCallback = sword.feat.view.ShopCallback;

  export class User extends Feature {

    constructor(private _id: api.platform.User,
                private _account: Account,
                private _gift: Gift,
                private _player: Player,
                private _shop: Shop,
                private _navigation: Navigation,
                private _keyboard: Keyboard,
                private _explorer: Explorer,
                private _invite: Invite) {

      super([_account, _gift, _player, _shop, _navigation, _invite, _explorer]);
    }

    get id(): api.platform.User {
      return this._id;
    }

    get gift(): Gift {
      return this._gift;
    }

    get shop(): Shop {
      return this._shop;
    }

    get navigation(): Navigation {
      return this._navigation;
    }

    get keyboard(): Keyboard {
      return this._keyboard;
    }

    get player(): Player {
      return this._player;
    }

    get account(): Account {
      return this._account;
    }

    get explorer(): Explorer {
      return this._explorer;
    }

    get invite(): Invite {
      return this._invite;
    }

    init(): void {
      const complete = () => {
        this._navigation.page = Page.GAME;
      };

      this._account.check(() => {
        this._gift.check();
        this._explorer.check();
        this._explorer.user = this;
        this._invite.init();
        this._player.loadGames(() => {
          if (this._player.allGames.length === 0) {
            this._invite.accept();
            this.orderGame(() => {
              complete();
            });
          } else {
            complete();
          }
        });
      });
    }

    orderGame(complete: Callback): void {
      this._account.buyPuzzle(this._player, complete);
    }

    tryOrder(callback: ShopCallback, reason: string, price: number = 1) {
      if (this._account.letters < price) {
        this._shop.enter(reason, price - this._account.letters,
            (bought) => callback(bought && this._account.letters >= price));
      } else {
        callback(true);
      }
    }

    tillNewLetter(): number {
      return this._account.actions / sword.api.platform.actionsForStar();
    }
  }
}
