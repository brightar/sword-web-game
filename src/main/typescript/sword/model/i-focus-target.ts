namespace sword.model {

  export interface IFocusTarget {
    insertChar(code: string): void
    blur(): void
  }
}
