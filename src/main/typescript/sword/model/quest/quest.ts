namespace sword.model.quest {

  import Placement = sword.model.quest.quests.Placement;
  import Controls = sword.model.quest.quests.Controls;
  export class Quest {
    public static BASE_STYLE = 'quest';
    public static BASE_STYLE_PREFIX = Quest.BASE_STYLE + '--';

    protected _steps: IStep[] = [];
    protected _reason: string = '';

    private _rewardReady: boolean = false;

    constructor(private _name: string,
                private _step: number,
                private _rewardCondition: (user: User) => boolean,
                private _prize: number) {
    }

    get name(): string {
      return this._name;
    }

    get step(): IStep {
      if (this._step > this._steps.length - 1) {
        this._step = 0;
      }
      return this._rewardReady ? Quest._reward(this._prize, this._reason) :
          this._steps[this._step];
    }

    next(user: User): boolean {
      this._rewardReady = this.rewardReady(user);
      if (!this._rewardReady) {
        const next = this.step.next(user);
        if (next) {
          this._step += 1;
          if (this.step) {
            this._steps[this._step].start(user);
          }
        }

        return next;
      }

      return false;
    }

    rewardReady(user: User): boolean {
      this._rewardReady = this._rewardCondition(user);
      return this._rewardReady;
    }

    private static _reward(prize: number,
                           text: string,
                           prepare: (user: User) => void = () => {}): IStep {
      return new Step((user) =>
      '<h2>Позравляем! ' + prize + ' <span class="star"></span> Ваши! </h2><h3>' + text + '</h3>' +
      '<div class = "quest__body__button">Ура!</div>', prepare, () => true, {
        highlighted: ['.quest'],
        event: '',
        placement: Placement.CENTER,
        controls: [Controls.REWARD]
      }, true);
    }
  }
}
