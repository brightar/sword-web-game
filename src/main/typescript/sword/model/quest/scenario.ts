namespace sword.model.quest {

  import Callback = brightar.Callback;
  export class Scenario implements IStep {

    private _current: number = 0;

    constructor(private _steps: IStep[]) {}

    content(user: User): string {
      return this._steps[this._current] ?
          this._steps[this._current].content(user) : '';
    }

    highlighted(): string[] {
      return this._steps[this._current] ?
          this._steps[this._current].highlighted() : [];
    }

    style(): string {
      return this._steps[this._current] ?
          this._steps[this._current].style() : '';
    }

    isActive(user: User): boolean {
      return this._steps[this._current] ?
          this._steps[this._current].isActive(user) : false;
    }

    next(user: User): boolean {
      this._current += 1;
      const finish = this._current === this._steps.length;
      if (!finish) {
        this._steps[this._current].start(user);
      }
      return finish;
    }

    start(user: User): void {
      if (this._steps[this._current] !== undefined) {
        this._steps[this._current].start(user);
      }
    }

    get completed(): boolean {
      return this._steps[this._current] !== undefined &&
          this._steps[this._current].completed;
    }
  }
}
