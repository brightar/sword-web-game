namespace sword.model.quest {

  import Callback = brightar.Callback;

  export interface IStep {
    content(user: User): string
    highlighted(): string[]
    style(): string
    isActive(user: User): boolean

    start(user: User): void
    next(user: User): boolean
    completed: boolean;
  }
}
