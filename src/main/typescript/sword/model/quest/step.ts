namespace sword.model.quest {

  import Callback = brightar.Callback;

  export type StepOptions = {
    highlighted: string[],
    event: string,
    placement: string,
    controls: string[]
  };

  export type Prepare = (user: User) => void;
  export type Precondition = (user: User) => boolean;

  export class Step implements IStep {
    private _styles: string[] = [];

    constructor(private _content: (user: User) => string,
                private _prepare: Prepare,
                private _precondition: Precondition,
                private _options: StepOptions,
                private _completed: boolean = false) {
      this._styles = _options.controls;
      this._styles.push(_options.placement);
    }

    isActive(user: User): boolean {
      return this._precondition(user);
    }

    content(user: User): string {
      return this._content(user);
    }

    highlighted(): string[] {
      return this._options.highlighted;
    }

    next(user: User): boolean {
      return true;
    }

    start(user: User): void {
      this._prepare(user);
      if (this._options.event !== '') {
        user.explorer.wait(this._options.event, () => {
          user.explorer.next();
        });
      }
    }

    style(): string {
      return this._stylize(this._styles);
    }

    get completed(): boolean {
      return this._completed;
    }

    private _stylize(styles: string[]): string {
      return Quest.BASE_STYLE + ' ' +
          styles.map((s) => Quest.BASE_STYLE_PREFIX + s)
                .reduce((s1, s2) => s1 + ' ' + s2, '');
    }
  }
}
