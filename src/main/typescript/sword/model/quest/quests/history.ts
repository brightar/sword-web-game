namespace sword.model.quest.quests {

  import STAR = sword.view.STAR;

  export class History extends Quest {
    static NAME: string = 'history';
    static EVENTS: string[] = [Event.GAME_ORDER, Event.HISTORY_OPEN,
      Event.GAME_CONTINUE];

    constructor(currentStep: number, prize: number) {
      super(History.NAME, currentStep,
            (user) => user.explorer.isExplored(History.EVENTS), prize);

      this._reason = 'Теперь вы знаете как переходить от одного сканворда к другому.';
      this._steps = [
        step((user) =>
            '<p>10 ' + STAR + ' можно обменять на новый сканворд. ' +
            'Попробуйте, это легко!</p>', {
          icon: 'quest/start/2-start.svg',
          highlighted: ['.header__next'],
          event: Event.GAME_ORDER,
          placement: Placement.UNDER_HEADER,
          controls: [Controls.HIDE]
        }, (user) => user.account.letters >= sword.api.platform.puzzlePrice()),

        scenario([
          step((user) => '<p>Поздравляем, вы получили новый сканворд. ' +
            'Теперь вернемся к предыдущему.</p>', {
            icon: 'quest/start/2-start.svg',
            highlighted: ['.header__history__back'],
            event: Event.HISTORY_OPEN,
            placement: Placement.UNDER_HEADER,
            controls: [Controls.HIDE]
          }, (user) => user.navigation.page === Page.GAME),

          step((user) => '<p>Теперь вернемся к предыдущему.</p>', {
            icon: 'quest/start/2-start.svg',
            highlighted: ['.grid-preview:nth-of-type(2)'],
            event: Event.GAME_CONTINUE,
            placement: Placement.HEADER
          })
        ])
      ];
    }
  }
}
