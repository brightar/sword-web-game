namespace sword.model.quest.quests {

  export class Controls {
    static HIDE: string = 'hide';
    static NEXT: string = 'next';
    static REWARD: string = 'reward';
  }
}
