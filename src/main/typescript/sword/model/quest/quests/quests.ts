namespace sword.model.quest.quests {

  import questReward = sword.api.platform.questReward;

  type SimpleOptions = { [key: string]: any };

  export const defPrepare: Prepare = () => {};
  export const defPrecondition: Precondition = (user) => true;

  export function create(progress: api.quest.State): Quest {
    const name = progress[0];
    const step = progress[1];
    const reward = questReward(name);
    switch (progress[0]) {
      case Introduction.NAME:
        return new Introduction(step, reward);

      case History.NAME:
        return new History(step, reward);
    }

    return null;
  }

  export function step(content: (user: User) => string,
                       options: SimpleOptions = {},
                       precondition: Precondition = defPrecondition,
                       prepare: Prepare = defPrepare): IStep {
    const contentNext = (user: User) => {
      return content(user) +
          '<div class = "quest__body__button">Продолжить</div>';
    };

    return new Step(options['event'] === undefined ? contentNext : content,
        prepare, precondition, {
      highlighted: (options['highlighted'] || []).concat('.quest'),
      event: options['event'] || '',
      placement: options['placement'] || Placement.CENTER,
      controls: options['controls'] || []
    });
  }

  export function scenario(steps: IStep[]): IStep {
    return new Scenario(steps);
  }
}
