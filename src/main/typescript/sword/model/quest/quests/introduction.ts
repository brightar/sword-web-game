namespace sword.model.quest.quests {

  import moreThan = sword.view.moreThan;

  export class Introduction extends Quest {
    static NAME: string = 'introduction';
    static EVENTS: string[] = [
      Event.CLUE_SELECTED, Event.CLUE_SOLVED, Event.STAR_PRESSED,
      Event.HINT_USED, Event.SHOP_OPEN, Event.PURCHASE];

    private static _preCondition = function(user: User): boolean {
      return user.navigation.page === Page.GAME &&
          user.player.solver !== null &&
          !user.player.solver.game.isWon;
    };

    constructor(currentStep: number, prize: number) {
      super(Introduction.NAME, currentStep,
            (user: User) => {
              const explored = user.explorer.isExplored(Introduction.EVENTS);
              if (explored) {
                user.shop.removeFake();
              }
              return explored;
            }, prize);

      this._reason = 'Теперь вы знаете как решать сканворды.';
      this._steps = [
        step((user) => `
              <h2>Добро пожаловать</h2>
              <p>В удивительный мир любимых головоломок «Сканворды&nbsp;—&nbsp;Игра&nbsp;Слов» !</p>
              <hr>
              <p>Это обучение всего на 5 минут и принесет вам подарок.</p>
              <p>Но если хотите — можете его пропустить.</p>`, {
              controls: [Controls.NEXT, Controls.HIDE]
            }, Introduction._preCondition),

        scenario([
          step((user) => `
              <p>Нажмите на любую клетку с вопросом —
                 и вы сможете ввести ответ.</p>`, {
            highlighted: ['.grid__cell--clue.grid__cell--not-solved'],
            event: Event.CLUE_SELECTED,
            placement: Placement.HEADER
          }, Introduction._preCondition),

          step((user) => '<p>Введите ответ, используя клавиатуру. ' +
            'Подсказка: ' + user.player.solver.game.getAnswer() + '.</p>', {
            highlighted: ['.grid__cell--current-word'],
            event: Event.CLUE_SOLVED,
            placement: Placement.HEADER
          }, Introduction._preCondition)
        ]),

        step((user) => '<p>Дальше — интереснее!</p>', {
          controls: [Controls.NEXT, Controls.HIDE]
        }, Introduction._preCondition),

        scenario([
          step((user) =>
              '<p>Звездочки нужны для подсказок! ' +
              'Нажмите, чтобы схватить одну.</p>', {
            highlighted: ['.header__menu-stars__stars'],
            event: Event.STAR_PRESSED,
            placement: Placement.UNDER_HEADER,
            controls: [Controls.HIDE]
          }, (user) => Introduction._preCondition(user) &&
                  user.account.letters > 0),

          step((user) =>
              '<p>Бросьте её на клетку.</p>', {
            highlighted: ['.grid__cell--letter:not(.grid__cell--disabled-letter)'],
            event: Event.HINT_USED,
            placement: Placement.HEADER
          }, (user) => Introduction._preCondition(user) &&
                  user.account.letters  > 0)
        ]),

        step((user) => '<p>У вас отлично получилось!</p>', {
          controls: [Controls.NEXT, Controls.HIDE]
        }, Introduction._preCondition),

        scenario([
          step((user) => `
              <p>Заберите Ваш подарок в магазине!</p>
              <hr />
              <p>Есть три способа увеличить запас звездочек:</p>
              <ul>
                <li>Купить звездочки в магазине.</li>
                <li>Отгадать 5 слов (+1 <span class="star"></span>).</li>
                <li>Отгадать сканворд целиком (+5 <span class="star"></span>).</li>
              </ul>`,{
            highlighted: ['.header__menu-stars__shop'],
            event: Event.SHOP_OPEN,
          }, Introduction._preCondition),

          step((user) => '<p>Заберите подарок!</p>', {
            highlighted: ['.shop'],
            placement: Placement.HEADER,
            event: Event.PURCHASE
          }, Introduction._preCondition, (user) => user.shop.addFake()),
        ])
      ];
    }
  }
}
