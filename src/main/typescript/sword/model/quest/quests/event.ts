namespace sword.model.quest.quests {

  export class Event {
    static CLUE_SELECTED: string = 'clue-selected';
    static CLUE_SOLVED: string = 'clue-solved';

    static STAR_PRESSED: string = 'star-pressed';
    static HINT_USED: string = 'hint-used';

    static PURCHASE: string = 'purchase';

    static GAME_ORDER: string = 'game-order';
    static GAME_CONTINUE: string = 'game-continue';
    static HISTORY_OPEN: string = 'history-open';
    static SHOP_OPEN: string = 'shop-open';
  }
}
