
namespace sword.model.quest.quests {
  export class Placement {
    static HEADER: string = 'header';
    static UNDER_HEADER: string = 'under-header';
    static CENTER: string = 'center';
  }
}
