namespace sword.model.view {

  export class ShopBundle {
    constructor(public serviceId: number,
                public lettersAmount: number,
                public price: number,
                public discount: number,
                public picUrl: string) {}
  }
}
