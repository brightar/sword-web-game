namespace sword.model.game {

  import log = brightar.log;

  export class State {

    constructor(private _field: Field,
                private _cells: Cell[] = []) {}

    getField(): Field {
      return this._field;
    }

    getChar(position: Position): Cell {
      return this._cells[position].toUpperCase() || '';
    }

    setChar(letter: Position, cell: Cell): State {
      if (cell !== '' && this.isAvailable(letter)) {
        this._cells[letter] = cell.toLowerCase().trim();
      }

      return this;
    }

    isFirstLetter(clue: Position, letter: Position): boolean {
      const word = this._field.getWord(clue);
      return !word.filter((pos) => pos < letter)
          .map((pos) => this.isAvailable(pos))
          .reduce((x, y) => x || y, false);
    }

    isAvailable(letter: Position): boolean {
      return this._cells[letter] === this._cells[letter].toLowerCase();
    }

    isOpened(letter: Position): boolean {
      return this._cells[letter].length === 1 &&
          this._cells[letter] === this._cells[letter].toUpperCase();
    }

    openLetter(letter: Position): State {
      if (this.isAvailable(letter)) {
        this._cells[letter] = this._field.getCell(letter).toUpperCase();
      }

      return this;
    }

    openWord(clue: Position): string {
      return this._field.getWord(clue).map((letter) => {
        this.openLetter(letter);
        return this.getChar(letter);
      }).join('');
    }

    isCorrect(clue: Position): boolean {
      const word: Position[] = this._field.getWord(clue);
      const value: string = word.map((letter) => this._cells[letter]).join('');
      const answer: string = word.map((letter) =>
          this._field.getCell(letter)).join('');

      return value.toLowerCase() === answer.toLowerCase();
    }

    solved(clue: Position): boolean {
      const word: Position[] = this._field.getWord(clue);

      return word.map((letter) => this._cells[letter] &&
          this._cells[letter] === this._cells[letter].toUpperCase())
          .reduce((a, b) => a && b, true);
    }

    getPuzzle(): api.game.Puzzle {
      return [
        this._field.getSize(),
        this._cells.map((c, p) => this.isOpened(p) ?
            c.toUpperCase() : this._field.getCell(p)).join(''),

        this._field.getClue()
      ]
    }
  }
}
