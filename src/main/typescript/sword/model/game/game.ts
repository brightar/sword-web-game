namespace sword.model.game {

  import util = brightar.util;
  import log = brightar.log;
  import Feature = brightar.feat.Feature;

  /**
   *  . → → x
   *  ↓ 0 1 2
   *  y 3 4 *
   *  ↓ 6 7 8
   *
   * p = x + y * width
   *
   * y = (p - x) / width = (p - p % width) / width
   * x = p - (y * width) = p % width
   */
  export type Position = number;
  export type Coordinate = [number, number];

  export type Cell = string;
  export type Direction = boolean;

  export const HORIZONTAL: Direction = true;
  export const VERTICAL: Direction = false;

  export const LETTERS: Cell[] = [
    'Й', 'Ц', 'У', 'К', 'Е', 'Н', 'Г', 'Ш',
    'Х', 'Ъ', 'Ф', 'Ы', 'В', 'А', 'П', 'Р',
    'О', 'Л', 'Д', 'Ж', 'Э', 'Я', 'Ч', 'С',
    'М', 'И', 'Т', 'Ь', 'Б', 'Ю', 'Щ', 'З'
  ];


  export function grid(): Position[] {
    return new Array(90).map((_, i) => i);
  }

  export class Game {
    private _direction: Direction = VERTICAL;
    private _letter: Position = -1;

    constructor(private _id: api.game.Id,
                private _state: State,
                private _index: number,
                private _touchTime: number) {}

    get id(): api.game.Id {
      return this._id;
    }

    get index(): number {
      return this._index;
    }

    get state(): State {
      return this._state;
    }

    get field(): Field {
      return this._state.getField()
    };

    get touchedAt(): number {
      return this._touchTime;
    }

    get solveRatio(): number {
      const clues: number[] = Object
          .keys(this.field.getClue())
          .map((key) => Number(key));

      return clues.filter((clue) =>
              this._state.isCorrect(clue)).length / clues.length
    }

    get puzzle(): api.game.Puzzle {
      return this._state.getPuzzle();
    }

    open(): void {
      this._touchTime = Math.floor(Date.now() / 1000);
    }

    getAnswer(clue: Position = this._currentClue): string {
      return this.field.getWord(clue).map((p) => this.field.getCell(p)).join('')
    }

    isCurrentLetter(position: Position): boolean {
      return this._letter === position;
    }

    isCurrentWord(position: Position): boolean {
      if (this.field.isLetter(position)) {
        return this.field.getWord(this._currentClue).indexOf(position) >= 0
      }

      return position === this._currentClue;
    }

    get isWon(): boolean {
      const len: number = this.field.getWidth() * this.field.getHeight() - 1;
      return util.range(0, len)
          .map((position) =>
          this._state.isOpened(position) ||
          this.field.getCellType(position) === Field.CLUE)
          .reduce((x, y) => x && y, true);
    }

    selectWord(clue: Position, first: boolean = false): void {
      const word: Position[] = this.field.getWord(clue);
      const direction: Direction = this.field.getDirection(clue);

      if (this._direction !== direction) {
        this._direction = direction;
      }

      if (first || word.indexOf(this._letter) === -1) {
        this._letter = this._getFirstAvailableLetter(word);
      }
    }

    selectNext(direction: Direction = this._direction): void {
      this._direction = direction;

      const next: Position = this.field.getNextLetter(this._letter, direction);
      const word: Position[] = this.field.getWord(this._currentClue);

      const canMove: boolean = word
          .filter((pos) => pos > this._letter)
          .map((pos) => this._state.isAvailable(pos))
          .reduce((x, y) => x || y, false);

      if (canMove && word.indexOf(next) >= 0) {
        this.selectLetter(next);
      }
    }

    selectPrev(direction: Direction = this._direction): void {
      this._direction = direction;
      this.selectLetter(this.field.getPrevLetter(this._letter, direction));
    }

    selectLetter(letter: Position, rotate: boolean = true): void {
      const allClues: Position[] = [];

      const currentDirClue: Position = this.field.getCluePosition(
          letter, this._direction);

      if (currentDirClue !== -1 && !this._state.isCorrect(currentDirClue)) {
        allClues.push(currentDirClue)
      }

      const anotherDirClue: Position = this.field.getCluePosition(
          letter, !this._direction);

      if (anotherDirClue !== -1 && !this._state.isCorrect(anotherDirClue)) {
        allClues.push(anotherDirClue)
      }

      if (allClues.length > 0 && this._letter !== letter) {
        if (allClues.map((clue) =>
                this.isCurrentWord(clue)).indexOf(true) === -1) {

          const firstLetterClues:Position[] = allClues.filter((clue) =>
              this._state.isFirstLetter(clue, letter));

          if (firstLetterClues.length > 0) {
            this.selectWord(firstLetterClues.shift());
          } else {
            this.selectWord(allClues.shift());
          }
        }

        this._letter = letter;
      } else if (rotate && allClues.indexOf(anotherDirClue) >= 0) {
        this.selectWord(anotherDirClue);
      }
    }

    clearSelection(): void {
      if (this._letter >= 0) {
        this._letter = -1;
      }
    }

    openLetter(position: Position): void {
      this.selectLetter(position);
      this._state.openLetter(position);
      this._markSolved();
    }

    insertChar(input: Cell): string[] {
      if (this._letter !== -1) {
        if (input === ' ') {
          this._state.setChar(this._letter, input);
          this.selectPrev();
        } else {
          this._state.setChar(this._letter, input);
          return this._markSolved();
        }
      }

      return [];
    }

    private _markSolved(): string[] {
      var words: string[] = [];

      const currentClue: Position = this._currentClue;
      const anotherClue: Position = this._anotherClue;

      if (this._state.isCorrect(currentClue) &&
          !this._state.solved(currentClue)) {

        this._letter = -1;

        words.push(this._state.openWord(currentClue));
      } else {
        this.selectNext();
      }

      if (this._state.isCorrect(anotherClue) &&
          !this._state.solved(anotherClue)) {

        words.push(this._state.openWord(anotherClue));
      }

      if (this.isWon) {
        this._letter = -1;
      }

      return words;
    }

    private get _currentClue(): Position {
      return this.field.getCluePosition(this._letter, this._direction);
    }

    private get _anotherClue(): Position {
      return this.field.getCluePosition(this._letter, !this._direction);
    }

    private _getFirstAvailableLetter(word: Position[]): Position {
      for (var pos of word) {
        if (this._state.isAvailable(pos)) {
          return pos;
        }
      }

      return -1;
    }
  }
}
