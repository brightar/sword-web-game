namespace sword.model.game {

  /**
   * vertical   horizontal
   *
   *  0   6      6 5 4 →
   *  1 * 5        * 3 →
   *  2 3 4      0 1 2 →
   *  ↓ ↓ ↓
   */
  export class Arrow  {

    static H0: Cell = String.fromCharCode(100);
    static H1: Cell = String.fromCharCode(101);
    static H2: Cell = String.fromCharCode(102);
    static H3: Cell = String.fromCharCode(103);
    static H4: Cell = String.fromCharCode(104);
    static H5: Cell = String.fromCharCode(105);
    static H6: Cell = String.fromCharCode(106);

    static V0: Cell = String.fromCharCode(107);
    static V1: Cell = String.fromCharCode(108);
    static V2: Cell = String.fromCharCode(109);
    static V3: Cell = String.fromCharCode(110);
    static V4: Cell = String.fromCharCode(111);
    static V5: Cell = String.fromCharCode(112);
    static V6: Cell = String.fromCharCode(113);

    static getType(cell: Cell): string {
      switch (cell) {
        case Arrow.V0: return 'v0';
        case Arrow.V1: return 'v1';
        case Arrow.V2: return 'v2';
        case Arrow.V3: return 'v3';
        case Arrow.V4: return 'v4';
        case Arrow.V5: return 'v5';
        case Arrow.V6: return 'v6';
        case Arrow.H0: return 'h0';
        case Arrow.H1: return 'h1';
        case Arrow.H2: return 'h2';
        case Arrow.H3: return 'h3';
        case Arrow.H4: return 'h4';
        case Arrow.H5: return 'h5';
        case Arrow.H6: return 'h6';
      }

      return '';
    }

    static getDirection(cell: Cell): Direction {
      switch (cell) {
        case Arrow.V0:
        case Arrow.V1:
        case Arrow.V2:
        case Arrow.V3:
        case Arrow.V4:
        case Arrow.V5:
        case Arrow.V6:
          return VERTICAL;
      }

      return HORIZONTAL
    }

    static isArrow(cell: Cell): boolean {
      switch (cell) {
        case Arrow.V0:
        case Arrow.V1:
        case Arrow.V2:
        case Arrow.V3:
        case Arrow.V4:
        case Arrow.V5:
        case Arrow.V6:
        case Arrow.H0:
        case Arrow.H1:
        case Arrow.H2:
        case Arrow.H3:
        case Arrow.H4:
        case Arrow.H5:
        case Arrow.H6: return true
      }

      return false;
    }

    static getWordStart(cell: Cell, coordinate: Coordinate): Coordinate {
      const [x, y]: Coordinate = coordinate;

      switch (cell) {
        case Arrow.V0: return [x - 1, y - 1];
        case Arrow.V1: return [x - 1, y    ];
        case Arrow.V2: return [x - 1, y + 1];
        case Arrow.V3: return [x    , y + 1];
        case Arrow.V4: return [x + 1, y + 1];
        case Arrow.V5: return [x + 1, y    ];
        case Arrow.V6: return [x + 1, y - 1];
        case Arrow.H0: return [x - 1, y + 1];
        case Arrow.H1: return [x    , y + 1];
        case Arrow.H2: return [x + 1, y + 1];
        case Arrow.H3: return [x + 1, y    ];
        case Arrow.H4: return [x + 1, y - 1];
        case Arrow.H5: return [x    , y - 1];
        case Arrow.H6: return [x - 1, y - 1];
      }

      return [-1, -1];
    }
  }
}
