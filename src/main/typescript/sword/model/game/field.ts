namespace sword.model.game {

  type ClueWord = { [clue: number] : Position[] }
  type DirectionClue = { [direction: number]: Position }
  type LetterClues = { [letter: number] : DirectionClue }

  export class Field {

    static CLUE: string = 'clue';
    static LETTER: string = 'letter';
    static ARROW: string = 'arrow';

    static getArrowType(cell: Cell): string {
      return Field.ARROW + '-' + Arrow.getType(cell);
    }

    private _clueWord: ClueWord = {};
    private _letterClues: LetterClues = {};

    constructor(private _width: number = 0,
                private _height: number = 0,
                private _cells: Cell[] = [],
                private _clues: api.game.Clue = {}) {

      for (var key in this._clues) {
        const clue: Position = Number(key);
        const direction: Direction = this.getDirection(clue);
        const word: Position[] = this.getWord(clue);

        for (var letter of word) {
          if (this._letterClues[letter] === undefined) {
            this._letterClues[letter] = {};
          }

          this._letterClues[letter][Number(direction)] = clue;
        }
      }
    }

    getSize(): api.game.Size {
      return [this._width, this._height];
    }

    getClue(): api.game.Clue {
      return this._clues;
    }

    getGrid(): Position[] {
      return this._cells.map((_, p) => p);
    }

    getLetters(): Position[] {
      return this.getGrid().filter((p) => this.isLetter(p));
    }

    getClues(): Position[] {
      return this.getGrid().filter((p) => !this.isLetter(p));
    }

    getWidth(): number {
      return this._width;
    }

    getHeight(): number {
      return this._height;
    }

    getCell(position: Position): Cell {
      return this._cells[position] || '';
    }

    getY(position: Position): number {
      return Math.floor(position / this._width)
    }

    getX(position: Position): number {
      return position % this._width
    }

    getNextLetter(position: Position, direction: Direction): Position {
      const next: Position = this._getPosition(
          this._shift(this._getCoordinate(position), direction, 1));

      if (next !== position && this.isLetter(next)) {
        return next;
      }

      return -1;
    }

    getPrevLetter(position: Position, direction: Direction): Position {
      const coordinate = this._shift(
          this._getCoordinate(position), direction, -1);
      if (coordinate[0] >= 0) {
        const prev: Position = this._getPosition(coordinate);

        if (this.isLetter(prev)) {
          return prev;
        }
      }

      return position;
    }

    getCluePosition(letter: Position, direction: Direction): Position {
      const clues: DirectionClue = this._letterClues[letter];
      if (clues !== undefined) {
        const prefer: Position = clues[Number(direction)];
        const other: Position = clues[Number(!direction)];

        if (prefer !== undefined) return prefer;
        else if (other !== undefined) return other;
        else return -1;
      }

      return -1;
    }

    getDirection(clue: Position): Direction {
      return Arrow.getDirection(this.getCell(clue))
    }

    getWord(clue: Position): Position[] {
      if (this._clueWord[clue] === undefined) {
        const direction: Direction = this.getDirection(clue);
        const start: Coordinate = Arrow.getWordStart(
            this.getCell(clue), this._getCoordinate(clue));

        this._clueWord[clue] = this._slice(start, direction).map(
            (coordinate) => this._getPosition(coordinate));
      }

      return this._clueWord[clue];
    }

    getWording(position: Position): string {
      return this._clues[position] || '';
    }

    isLetter(position: Position): boolean {
      return this._clues[position] === undefined;
    }

    getLetterArrows(letter: Position): string[] {
      const arrows: string[] = [];

      for (var direction in this._letterClues[letter]) {
        const clue: Position = this._letterClues[letter][direction];
        const word: Position[] = this.getWord(clue);

        if (word[0] === letter) {
          arrows.push(Field.getArrowType(this.getCell(clue)))
        }
      }

      return arrows;
    }

    getCellType(position: Position): string {
      if (this._clues[position] !== undefined) {
        return Field.CLUE;
      } else {
        return Field.LETTER;
      }
    }

    private _getCoordinate(position: Position): Coordinate {
      return [this.getX(position), this.getY(position)]
    }

    private _getPosition(coordinate: Coordinate): number {
      const [x, y]: Coordinate = coordinate;

      return y * this._width + x
    }

    private _shift(coordinate: Coordinate,
                   direction: Direction,
                   shift: number): Coordinate {

      const [x, y]: Coordinate = coordinate;

      switch (direction) {
        case HORIZONTAL:
          if (x + shift < this._width) {
            return [x + shift, y]
          }

          break;

        case VERTICAL:
          if (y + shift < this._height) {
            return [x, y + shift];
          }

          break;
      }

      return coordinate;
    }

    private _slice(coordinate: Coordinate,
                   direction: Direction,
                   shift: number = 0,
                   result: Coordinate[] = []): Coordinate[] {

      const next: Coordinate =
          this._shift(coordinate, direction, shift);

      if (next !== coordinate && this.isLetter(this._getPosition(next))) {
        result.push(next);
        return this._slice(coordinate, direction, shift + 1, result);
      }

      return result;
    }
  }
}
