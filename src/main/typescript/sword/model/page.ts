namespace sword.model {
  export class Page {
    static HISTORY: string = 'history';
    static GAME: string = 'game';
    static PRELOADER: string = 'preloader';
  }
}
