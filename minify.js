var fs = require('fs');

function shell(cmd, callback) {
  var exec = require('child_process').exec;
  var proc = exec(cmd);
  var data = '';
  var dataCollector = function(chunk) { data += chunk };
  proc.stdout.on('data', dataCollector);
  proc.on('close', function() {
    callback(data);
  });
}

function compress(compressor, input, output, type, callback) {
  compressor.compress(input, {
    'type': type,
    'line-break': 1024
  }, function(error, minified, warnings) {
    if (error) {
      console.error(error);
      callback(false);
    } else {
      fs.writeFileSync(output, minified);
      console.warn(warnings);
      callback(true);
    }
  });
}

shell('npm ls', function(output) {
  if (output.indexOf('yuicompressor') < 0) {
    console.log('Please install yuicompressor.\nJust run `npm install`.');
  } else {

    var compressor = require('yuicompressor');

    const FOLDER = 'target/web-game/debug/';
    const JS_IN = FOLDER + 'app.js';
    const JS_OUT = FOLDER + 'app.min.js';
    const CSS_IN = FOLDER + 'app.css';
    const CSS_OUT = FOLDER + 'app.min.css';

    console.log('Minifying js...');
    compress(compressor, JS_IN, JS_OUT, 'js', function(jsSuccess) {
      console.log(jsSuccess ? 'DONE.' : 'ERROR!');
      console.log('\n');
      console.log('Minifying css...');
      compress(compressor, CSS_IN, CSS_OUT, 'css', function(cssSuccess) {
        console.log(cssSuccess ? 'DONE.' : 'ERROR!');

        if (cssSuccess && jsSuccess) {
          fs.unlinkSync(JS_IN);
          fs.unlinkSync(CSS_IN);
          fs.renameSync(JS_OUT, JS_IN);
          fs.renameSync(CSS_OUT, CSS_IN);
          console.log('Minified successfully.');
        } else {
          console.error('Minification unsuccessful.');
          process.exit(1);
        }
      });
    });
  }
});
