
const gulp = require('gulp');
const svgSprite = require('gulp-svg-sprite');
const fs = require('fs');


/**
 * NOP function
 */
const nop = function() {};


/**
 * @param {string} cmd
 * @param {function()} callback
 * @param {function(string)=} opt_printer
 */
const shell = function(cmd, callback, opt_printer) {
  const exec = require('child_process').exec;
  const proc = exec(cmd);
  const printer = opt_printer || function() {};
  proc.stdout.on('data', printer);
  proc.stderr.on('data', printer);
  proc.on('close', function() {
    callback();
  });
};


/**
 * @param {!Function} func
 * @param {number=} opt_times
 * @return {!Function}
 */
const times = function(func, opt_times) {
  var called = 0;
  const times = opt_times || 1;
  return function() {
    if (called < times) {
      called += 1;
      func.apply(null, arguments);
    }
  }
};


const noNpmConsole = function(line) {
  const MARKERS = [
      'npm',
      'ERR!',
      'This is most likely a problem with the ',
      'There is likely additional logging output above.',
      'Failed at the ',
      'Darwin',
      'ELIFECYCLE',
      'node v',
      'You can get their info via',
      'lessc src/main'
  ];

  const noNpm = MARKERS.map(function(marker) {
    return line.indexOf(marker) === -1;
  }).reduce(function(r1, r2) {
    return r1 && r2;
  }, true);

  if (noNpm) {
    console.log(line);
  }
};


/**
 * Build application HTML template.
 */
gulp.task('html', function(callback) {
  const DIR = 'src/main/jinja/';
  const DEST = 'target/web-game/debug/';
  const FILES = fs.readdirSync(DIR).filter(function(name) {
    return /\.tmpl$/.test(name);
  });

  var completed = 0;

  if (FILES.length > 0) {
    FILES.forEach(jinja);
  } else {
    callback();
  }

  function jinja(fileName) {
    const output = fileName.replace('tmpl', 'html');
    if (output.length > 0) {
      shell('j2 ' + DIR + fileName + ' package.json > ' + DEST + output, complete);
    }
  }

  function complete() {
    if (++completed >= FILES.length) callback();
  }
});


/**
 * Reinstall TypeScript dependencies.
 */
gulp.task('tsd', function(callback) {
  shell('npm run prepare', callback);
});


/**
 * Build JS from TypeScript sources.
 */
gulp.task('js', function(callback) {
  shell('npm run js', callback, function(line) {
    if (line.indexOf('TS') >= 0) {
      console.log(line);
    }
  });
});


/**
 * Build CSS from LESS files.
 */
gulp.task('css', function(callback) {
  shell('npm run css', callback, noNpmConsole);
});


/**
 * Copy assets.
 */
gulp.task('assets', function(callback) {
  shell('npm run assets', callback);
});


/**
 * Build svgs.
 */
gulp.task('svg', function() {
  const config = { mode: {
    symbol: {
      dest: 'src/main/assets/',
      sprite: 'svg.svg',
      inline: true,
      example: true
    }
  }};

  return gulp.src('src/main/assets/svg/**/*.svg')
      .pipe(svgSprite(config))
      .pipe(gulp.dest('.'));
});


/**
 * Auto rebuild project on files change.
 */
gulp.task('watch', ['default'], function() {
  shell('hs ./target & ', nop, times(console.log, 4));
  shell('tsc -w', nop, console.log);

  gulp.watch(['src/main/jinja/*.tmpl'], ['html']);
  gulp.watch('tsd.json', ['tsd']);
  gulp.watch('src/main/less/**/*.less', ['css']);
  gulp.watch('src/main/assets/**/*', ['assets', 'css']);
  gulp.watch('src/main/assets/sword/*/**/*.svg', ['svg']);
});


gulp.task('default', ['html', 'tsd', 'js', 'css', 'assets']);
